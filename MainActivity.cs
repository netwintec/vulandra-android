﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Widget;
using Android.OS;
using Android.Graphics;
using Android.Views;
using Android.Content.PM;
using Android.Content;
using RestSharp;
using Newtonsoft.Json;
using Square.Picasso;
using Android.Support.V7.Widget;
using Android.Animation;
using Android.Webkit;
using Firebase.Iid;

namespace Vulandra
{
	[Activity(ScreenOrientation = ScreenOrientation.Portrait)]
	public class MainActivity : Activity, View.IOnTouchListener, GestureDetector.IOnGestureListener
	{

		GlobalStructureInstance GbsInstance = GlobalStructureInstance.Instance;

		Typeface tfMB, tfOB, tfOR, tfOSB;

        RelativeLayout MenuClick, BackClick;
        ImageView MenuImg, BackImg;
		MenuView menu;

		RelativeLayout SliderView;
        RelativeLayout SwipePlaceholder;
		ViewFlipper immagineSwipe;
		protected GestureDetector gestureScanner;
		int attivo = 1;
		List<string> ListTitoli = new List<string>();
		List<ImageView> ListPallini = new List<ImageView>();
		TextView SliderTitle;
		LinearLayout PalliniContainer;


		private static int SWIPE_MIN_DISTANCE = 120;
		private static int SWIPE_MAX_OFF_PATH = 250;
		private static int SWIPE_THRESHOLD_VELOCITY = 200;
		List<SliderObj> listSlider;


        RelativeLayout Container;


        List<OfferteObj> listOfferte = new List<OfferteObj>();
        RecyclerView OfferteRecycler;
        OfferteAdapter offertaAdapter;
        LinearLayoutManager OfferteLM;
        TextView NessunaOffertaTxt;
        ProgressBar LoadingOfferte;

        RelativeLayout WebViewContainer;
        ValueAnimator WebViewContAnimator;
        int WidhtPixels;
        bool WebViewContOpen = false;

        WebView Web;
        ProgressBar Loading;

        public static MainActivity Instance { private set; get; }

		protected override void OnResume()
		{
			GbsInstance.SetPage(PageName.Home);

			base.OnResume();
		}

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			Window.RequestFeature(WindowFeatures.NoTitle);

			SetContentView(Resource.Layout.Main);

			GbsInstance.SetPage(PageName.Home);

			Instance = this;

			if (GetString(Resource.String.google_app_id) == "YOUR-APP-ID")
                throw new System.Exception("Invalid google-services.json file.  Make sure you've downloaded your own config file and added it to your app project with the 'GoogleServicesJson' build action.");


            //Console.WriteLine("InstanceID token: " + FirebaseInstanceId.Instance.Token);
            if (!string.IsNullOrEmpty(FirebaseInstanceId.Instance.Token))
            {
                SendToken(FirebaseInstanceId.Instance.Token, 0);
            }
                
			tfMB = Typeface.CreateFromAsset(Assets, "fonts/Montserrat_Bold.ttf");
			tfOB = Typeface.CreateFromAsset(Assets, "fonts/OpenSans_Bold.ttf");
			tfOR = Typeface.CreateFromAsset(Assets, "fonts/OpenSans_Regular.ttf");
			tfOSB = Typeface.CreateFromAsset(Assets, "fonts/OpenSans_SemiBold.ttf");

			InitMenuElement();

			// Get our button from the layout resource,

			MenuClick = FindViewById<RelativeLayout>(Resource.Id.MenuClick);
			MenuImg = FindViewById<ImageView>(Resource.Id.MenuIcon);

			MenuClick.Click += delegate
			{
				if (menu.MenuOpen)
				{
					menu.CloseMenu();
				}
				else
				{
					menu.OpenMenu();
				}
			};

            BackClick = FindViewById<RelativeLayout>(Resource.Id.BackClick);
            BackImg = FindViewById<ImageView>(Resource.Id.BackIcon);

            BackClick.Click += delegate
            {
                if (WebViewContOpen)
                {
                    ClickOnOffert();
                }

            };
            BackClick.Alpha = 0;

            SliderView = FindViewById<RelativeLayout>(Resource.Id.SliderView);

            SwipePlaceholder = FindViewById<RelativeLayout>(Resource.Id.SwipePlaceholder);
            SwipePlaceholder.Visibility = ViewStates.Visible;

			PalliniContainer = FindViewById<LinearLayout>(Resource.Id.PalliniContainer);

			SliderTitle = FindViewById<TextView>(Resource.Id.SliderTitle);
			SliderTitle.Typeface = tfMB;
		
			immagineSwipe = FindViewById<ViewFlipper>(Resource.Id.immagineSwipe);
			immagineSwipe.SetOnTouchListener(this);

			gestureScanner = new GestureDetector(this);

            FindViewById<TextView>(Resource.Id.ProposteTxt).Typeface = tfOSB;

            Container = FindViewById<RelativeLayout>(Resource.Id.Container);

            NessunaOffertaTxt = FindViewById<TextView>(Resource.Id.NessunaOffertaTxt);
            NessunaOffertaTxt.Typeface = tfMB;

            LoadingOfferte = FindViewById<ProgressBar>(Resource.Id.LoadingOfferte);
            LoadingOfferte.IndeterminateDrawable.SetColorFilter(Color.Rgb(102, 104, 162), PorterDuff.Mode.Multiply);

            OfferteRecycler = FindViewById<RecyclerView>(Resource.Id.OfferteRecycler);

            OfferteLM = new LinearLayoutManager(this);

            OfferteRecycler.SetLayoutManager(OfferteLM);

            offertaAdapter = new OfferteAdapter(listOfferte, this);

            OfferteRecycler.SetAdapter(offertaAdapter);

            OfferteRecycler.Visibility = ViewStates.Gone;
            LoadingOfferte.Visibility = ViewStates.Visible;
            NessunaOffertaTxt.Visibility = ViewStates.Gone;


            var metrics = Resources.DisplayMetrics;
            WidhtPixels = metrics.WidthPixels;


            WebViewContainer = FindViewById<RelativeLayout>(Resource.Id.WebViewContainer);
            var lp = (RelativeLayout.LayoutParams)WebViewContainer.LayoutParameters;
            lp.LeftMargin = WidhtPixels;
            WebViewContainer.LayoutParameters = lp;

            WebViewContAnimator = ValueAnimator.OfInt(1);
            WebViewContAnimator.SetInterpolator(new Android.Views.Animations.AccelerateDecelerateInterpolator());
            WebViewContAnimator.SetDuration(400);
            WebViewContAnimator.Update += (sender, e) => 
            {
                var val = (int)e.Animation.AnimatedValue;

                var lpAft = (RelativeLayout.LayoutParams)WebViewContainer.LayoutParameters;
                lpAft.LeftMargin = (int)(WidhtPixels * (val/100f));
                WebViewContainer.LayoutParameters = lpAft;

                BackClick.Alpha = ((100 - val) / 100f);
            };
            WebViewContAnimator.AnimationEnd += (sender, e) =>
            {
                
            };

            Web = FindViewById<WebView>(Resource.Id.OffertaWeb);
            Loading = FindViewById<ProgressBar>(Resource.Id.Loading);

            Web.LayoutParameters = new RelativeLayout.LayoutParams(metrics.WidthPixels,metrics.HeightPixels);

            Web.Visibility = ViewStates.Gone;

            Web.Settings.JavaScriptEnabled = true;
            Web.Settings.PluginsEnabled = true;

          
            Web.SetWebViewClient(new WebViewClient());//new MyWebViewClientHome());
            Web.SetWebChromeClient(new MyWebViewChromeHome());
            //chiamata solo per header PUSH
            //Web.LoadUrl("http://www.vulandraviaggi.com/wp-json/apnwp/register?os_type=android&device_token=1234567890");

            Loading.IndeterminateDrawable.SetColorFilter(Color.Rgb(102, 104, 162), PorterDuff.Mode.Multiply);


			new System.Threading.Thread(new System.Threading.ThreadStart(() =>
			{
				DownloadSliderData(0);
                DownloadDestinationData(0);
                DownloadOfferteData(0);
			})).Start();

           
			// INIT MENU

			menu = FindViewById<MenuView>(Resource.Id.menu);
			menu.SetBackgroundColor(Color.Argb(75, 0, 0, 0));
			menu.SetMenuColor(Color.Rgb(102, 104, 162));
			menu.PAGE_TAG = "0";
			menu.CellHeight = 50;
			menu.CellWidht = 225;
			menu.MenuImg = MenuImg;

			menu.AddElement(GbsInstance.ListMenuElement);

		}


        public void ClickOnOffert(int position = -1)
        {
            if(!WebViewContAnimator.IsRunning)
            {

                if (WebViewContOpen)
                    WebViewContAnimator.SetIntValues(0, 100);
                else
                    WebViewContAnimator.SetIntValues(100, 0);
                
                WebViewContAnimator.Start();

                WebViewContOpen = !WebViewContOpen;

                if(position != -1){
                    Web.Visibility = ViewStates.Gone;

                    var url = listOfferte[position].acf.link;
                    //Web.LoadUrl("http://www.viaggiaresicuri.it/home/");
                    if(url.EndsWith(".pdf",StringComparison.CurrentCulture))
                    {
                        url = "https://www.unipg.it/files/pagine/410/4-PDF-A.pdf";
                        Web.LoadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url="+url);

                    }
                    else
                    {
                        Web.LoadUrl(listOfferte[position].acf.link);
                    }

                }
            }

        }

		public override void OnBackPressed()
		{
			if (menu.MenuOpen)
			{
				menu.CloseMenu();
			}
			else
			{

                if (WebViewContOpen)
                {
                    ClickOnOffert();
                }
                else
                {
                    Intent startMain = new Intent(Intent.ActionMain);
                    startMain.AddCategory(Intent.CategoryHome);
                    startMain.SetFlags(ActivityFlags.NewTask);
                    StartActivity(startMain);
                }
			}

			//base.OnBackPressed();
		}

		public void DownloadSliderData(int time)
		{

			var client = new RestClient(GbsInstance.BaseUrl);

			var requestN4U = new RestRequest("wp-json/wp/v2/slide", Method.GET);

			//Console.WriteLine(client.BaseUrl+""+requestN4U.Resource);

			IRestResponse response = client.Execute(requestN4U);

			//Console.WriteLine("StatusCode v2/slide:" + response.StatusCode +
			//								  "\nContent v2/slide:" + response.Content);

			if (response.StatusCode == System.Net.HttpStatusCode.OK)
			{
				//var StrResponse = response.Content.Substring(1, response.Content.Length - 2);
				listSlider = JsonConvert.DeserializeObject<List<SliderObj>>(response.Content);
			}
			else
			{
				listSlider = new List<SliderObj>();
			}

			foreach (var obj in listSlider)
			{

				if (obj.pyre_heading.Contains("<img src"))
					obj.pyre_heading = "VULALIS";


				var client2 = new RestClient(GbsInstance.BaseUrl);

				////Console.WriteLine(GbsInstance.BaseUrl+"wp-json/wp/v2/media/"+obj.featured_media);

				var requestN4U2 = new RestRequest("wp-json/wp/v2/media/"+obj.featured_media, Method.GET);

				IRestResponse response2 = client2.Execute(requestN4U2);

				//Console.WriteLine("StatusCode v2/media:" + response2.StatusCode +
				//											  "\nContent v2/media:" + response2.Content);

				if (response2.StatusCode == System.Net.HttpStatusCode.OK)
				{
					//var StrResponse = response2.Content.Substring(1, response2.Content.Length - 2);
					var Slobj = JsonConvert.DeserializeObject<SliderObj>(response2.Content);
					obj.media_details = Slobj.media_details;
				}
				else
				{
					var md = new MediaDetails();
					md.file = "";
					obj.media_details = md;
				}

				//Console.WriteLine("SLOBJ:" + obj.pyre_heading + "|" + obj.pyre_caption + "|" + obj.featured_media + "|" + obj.media_details.file);
			}

			RunOnUiThread(() =>
			{
                //DownloadData(Index + 1);
                SwipePlaceholder.Visibility = ViewStates.Gone;
				foreach (var obj in listSlider)
				{
					//Console.WriteLine("ID:" + obj.id+" || http://www.vulandraviaggi.com/wp-content/"+obj.media_details.file);

					if (obj.pyre_button_1 != "")
					{
						var Image = new ImageView(ApplicationContext);
						Image.SetScaleType(ImageView.ScaleType.CenterCrop);

						Picasso.With(ApplicationContext)
						       .Load(GbsInstance.BaseImageUrl + obj.media_details.file)
						   .Into(Image);

						immagineSwipe.AddView(Image);

						Image.LayoutParameters = new ViewFlipper.LayoutParams(-1, -1);

						var Pallino = new ImageView(ApplicationContext);
						Pallino.SetImageResource(Resource.Drawable.PallinoPieno);

						PalliniContainer.AddView(Pallino);

						var LayoutParameters = new LinearLayout.LayoutParams(PixelsToDp(7), PixelsToDp(7));
						LayoutParameters.SetMargins(PixelsToDp(2), 0, PixelsToDp(2), 0);
						Pallino.LayoutParameters = LayoutParameters;
						ListPallini.Add(Pallino);

						ListTitoli.Add(obj.pyre_heading);
					}
				}

				cambiaPallino();
				return;
			});
		
		}

		public void DownloadDestinationData(int time)
		{

			var client = new RestClient(GbsInstance.BaseUrl);

			var requestN4U = new RestRequest("wp-json/wp/v2/avada_portfolio", Method.GET);

			//Console.WriteLine(client.BaseUrl + "" + requestN4U.Resource);

			IRestResponse response = client.Execute(requestN4U);

			//Console.WriteLine("StatusCode v2/avada_portfolio:" + response.StatusCode +
			//								  "\nContent v2/avada_portfolio:" + response.Content);

			var list = new List<DestinazioniObj>();
			if (response.StatusCode == System.Net.HttpStatusCode.OK)
			{
				//var StrResponse = response.Content.Substring(1, response.Content.Length - 2);
                list = JsonConvert.DeserializeObject<List<DestinazioniObj>>(response.Content);
			}

			foreach (var obj in list)
			{
				GbsInstance.ListDestinazioni.Add(obj.title.rendered);
			}
		}

        public void DownloadOfferteData(int time)
        {

            var client = new RestClient(GbsInstance.BaseUrl);
            //http://test.netwintec.com/wp-json/wp/v2/posts?categories=45
            var requestN4U = new RestRequest("wp-json/wp/v2/posts", Method.GET);
            requestN4U.AddQueryParameter("categories","45");
            //Console.WriteLine(client.BaseUrl + "" + requestN4U.Resource);

            IRestResponse response = client.Execute(requestN4U);

            //Console.WriteLine("StatusCode v2/posts?categories=45:" + response.StatusCode +
            //                  "\nContent v2/posts?categories=45:" + response.Content);

            var list = new List<OfferteObj>();
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                //var StrResponse = response.Content.Substring(1, response.Content.Length - 2);
                list = JsonConvert.DeserializeObject<List<OfferteObj>>(response.Content);
                RunOnUiThread(() =>
                {
                    foreach (var v in list)
                        listOfferte.Add(v);
                    offertaAdapter.NotifyDataSetChanged();

                    if(listOfferte.Count == 0)
                    {
                        OfferteRecycler.Visibility = ViewStates.Gone;
                        LoadingOfferte.Visibility = ViewStates.Gone;
                        NessunaOffertaTxt.Visibility = ViewStates.Visible;
                    }
                    else
                    {
                        OfferteRecycler.Visibility = ViewStates.Visible;
                        LoadingOfferte.Visibility = ViewStates.Gone;
                        NessunaOffertaTxt.Visibility = ViewStates.Gone;
                    }
                });

            }
            else
            {
				RunOnUiThread(() =>
				{
					OfferteRecycler.Visibility = ViewStates.Gone;
					LoadingOfferte.Visibility = ViewStates.Gone;
					NessunaOffertaTxt.Visibility = ViewStates.Visible;
				});
            }
        }

		private void InitMenuElement()
		{
			//****************************

			string CStxt = Resources.GetString(Resource.String.Menu_CS);

			MenuElement CS = new MenuElement(CStxt, Color.White, tfMB, Resource.Drawable.OrangeCircle, "1");
			CS.TextColor = Color.White;
			CS.Click += (sender, e) =>
			{
				//Console.WriteLine(CStxt);
				var intent = new Intent(this, typeof(ChiSiamoActivity));
				FinishActivityOnMenuChange();
				StartActivity(intent);

			};

			GbsInstance.ListMenuElement.Add(CS);

			//*********************************

			string VStxt = Resources.GetString(Resource.String.Menu_VS);

			MenuElement VS = new MenuElement(VStxt, Color.White, tfMB, Resource.Drawable.GreenCircle, "2");
			VS.TextColor = Color.White;
			VS.Click += (sender, e) =>
			{
				//Console.WriteLine(VStxt);
				var intent = new Intent(this, typeof(ViaggiareSicuriActivity));
				FinishActivityOnMenuChange();
				StartActivity(intent);
			};

			GbsInstance.ListMenuElement.Add(VS);

			//*********************************

			string VItxt = Resources.GetString(Resource.String.Menu_VI);

			MenuElement VI = new MenuElement(VItxt, Color.White, tfMB, Resource.Drawable.YellowCircle, "3");
			VI.TextColor = Color.White;
			VI.Click += (sender, e) =>
			{
				//Console.WriteLine(VItxt);
				var intent = new Intent(this, typeof(ViaggioIdealeActivity));
				FinishActivityOnMenuChange();
				StartActivity(intent);
			};

			GbsInstance.ListMenuElement.Add(VI);

			//*********************************

			string TVtxt = Resources.GetString(Resource.String.Menu_TV);

			MenuElement TV = new MenuElement(TVtxt, Color.White, tfMB, Resource.Drawable.PurpleCircle, "4");
			TV.TextColor = Color.White;
			TV.Click += (sender, e) =>
			{
				//Console.WriteLine(TVtxt);
				var intent = new Intent(this, typeof(IlTuoViaggioActivity));
				FinishActivityOnMenuChange();
				StartActivity(intent);
			};

			GbsInstance.ListMenuElement.Add(TV);

			//*********************************

			string Ctxt = Resources.GetString(Resource.String.Menu_C);

			MenuElement C = new MenuElement(Ctxt, Color.White, tfMB, Resource.Drawable.RedCircle, "5");
			C.TextColor = Color.White;
			C.Click += (sender, e) =>
			{
				//Console.WriteLine(Ctxt);
				var intent = new Intent(this, typeof(ContattiActivity));
				FinishActivityOnMenuChange();
				StartActivity(intent);
			};

			GbsInstance.ListMenuElement.Add(C);

			//*********************************

            //*********************************

            string Vtxt = Resources.GetString(Resource.String.Menu_V);

            MenuElement V = new MenuElement(Vtxt, Color.White, tfMB, Resource.Drawable.CyanCircle, "6");
            V.TextColor = Color.White;
            V.Click += (sender, e) =>
            {
                var url = "https://www.youtube.com/channel/UCiUlYAQvsa-v0_-5Y9uwuYw";
                Intent intent = null;
                try
                {
                    intent = new Intent(Intent.ActionView);
                    intent.SetPackage("com.google.android.youtube");
                    intent.SetData(Android.Net.Uri.Parse(url));
                    StartActivity(intent);
                }
                catch (ActivityNotFoundException ee)
                {
                    intent = new Intent(Intent.ActionView);
                    intent.SetData(Android.Net.Uri.Parse(url));
                    StartActivity(intent);
                }
            };

            GbsInstance.ListMenuElement.Add(V);

            //*********************************

		}

		public void FinishActivityOnMenuChange()
		{
            if (WebViewContOpen)
            {
                ClickOnOffert();
            }

			if (GbsInstance.isChiSiamo)
			{
				ChiSiamoActivity.Instance.Finish();
			}
			if (GbsInstance.isContatti)
			{
				ContattiActivity.Instance.Finish();
			}
			if (GbsInstance.isIlTuoViaggio)
			{
				IlTuoViaggioActivity.Instance.Finish();
			}
			if (GbsInstance.isViaggioIdeale)
			{
				ViaggioIdealeActivity.Instance.Finish();
			}
			if (GbsInstance.isViaggiareSicuri)
			{
				ViaggiareSicuriActivity.Instance.Finish();
			}

		}

		public bool OnTouch(View v, MotionEvent e)
		{

			return gestureScanner.OnTouchEvent(e);
		}

		public bool OnDown(MotionEvent e)
		{
			return true;
		}

		public bool OnFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
		{
			////Console.WriteLine(e1.GetX() + "  " + e2.GetX() + "  " + Math.Abs(e1.GetX() - e2.GetX()) + "  " + velocityX + "  " + Math.Abs(velocityX));
			try
			{
				if (e1.GetX() > e2.GetX() && Math.Abs(e1.GetX() - e2.GetX()) > SWIPE_MIN_DISTANCE && Math.Abs(velocityX) > SWIPE_THRESHOLD_VELOCITY)
				{
					if ((attivo + 1) != ListPallini.Count+1)
					{
						immagineSwipe.ShowNext();
						attivo++;
						cambiaPallino();
					}
				}
				else if (e1.GetX() < e2.GetX() && e2.GetX() - e1.GetX() > SWIPE_MIN_DISTANCE && Math.Abs(velocityX) > SWIPE_THRESHOLD_VELOCITY)
				{
					if ((attivo - 1) != 0)
					{
						immagineSwipe.ShowPrevious();
						attivo--;
						cambiaPallino();
					}
				}
			}
			catch (Exception e)
			{
			}
			return true;

		}

		public void cambiaPallino()
		{
			for (int i = 0; i < ListPallini.Count; i++)
			{
				if (i == attivo - 1)
				{
					ListPallini[i].SetImageResource(Resource.Drawable.PallinoPieno);
					SliderTitle.Text = ListTitoli[i];
				}
				else
				{
					ListPallini[i].SetImageResource(Resource.Drawable.PallinoVuoto);
				}
			}
		}

		public void OnLongPress(MotionEvent e)
		{
		}

		public bool OnScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
		{
			return true;
		}

		public void OnShowPress(MotionEvent e)
		{
		}

		public bool OnSingleTapUp(MotionEvent e)
		{
            if (listSlider.Count > 0)
            {
                if (SliderTitle.Text.Contains("VULALIS"))
                {
                    var url = "https://www.youtube.com/channel/UCiUlYAQvsa-v0_-5Y9uwuYw";
                    Intent intent = null;
                    try
                    {
                        intent = new Intent(Intent.ActionView);
                        intent.SetPackage("com.google.android.youtube");
                        intent.SetData(Android.Net.Uri.Parse(url));
                        StartActivity(intent);
                    }
                    catch (ActivityNotFoundException ee)
                    {
                        intent = new Intent(Intent.ActionView);
                        intent.SetData(Android.Net.Uri.Parse(url));
                        StartActivity(intent);
                    }
                }
                else if (SliderTitle.Text.Contains("OCCASIONI SPECIALI"))
                {
                    var url = "http://www.vulandraviaggi.com/2015/01/27/occasioni-speciali/";
                    Intent intent = new Intent(Intent.ActionView);
                    intent.SetData(Android.Net.Uri.Parse(url));
                    StartActivity(intent);
                }
                else if (SliderTitle.Text.Contains("4 ZAMPE"))
                {
                    var url = "http://www.vulandraviaggi.com/2015/01/27/4-zampe/";
                    Intent intent = new Intent(Intent.ActionView);
                    intent.SetData(Android.Net.Uri.Parse(url));
                    StartActivity(intent);
                }
                else if (SliderTitle.Text.Contains("SPOSAMI"))
                {
                    var url = "http://www.vulandraviaggi.com/2015/01/27/sposami/";
                    Intent intent = new Intent(Intent.ActionView);
                    intent.SetData(Android.Net.Uri.Parse(url));
                    StartActivity(intent);
                }
                else if (SliderTitle.Text.Contains("IL MONDO"))
                {
                    var intent = new Intent(this, typeof(ViaggioIdealeActivity));
                    FinishActivityOnMenuChange();
                    StartActivity(intent);
                }
            }
            return true;
		}
	
	
		private int PixelsToDp(float pixels)
		{
			return (int)Android.Util.TypedValue.ApplyDimension(Android.Util.ComplexUnitType.Dip, pixels, Resources.DisplayMetrics);
		}


        public void SendToken(string token, int time)
        {
            var client = new RestClient(GbsInstance.BaseUrl);
            //http://www.vulandraviaggi.com/wp-json/apnwp/register?os_type=android&device_token=1234567890
			var request = new RestRequest("wp-json/apnwp/register?os_type=android&device_token=" + token+ "&user_email_id=" + Android.OS.Build.Serial, Method.GET);

            //Console.WriteLine(client.BaseUrl + request.Resource);

            client.ExecuteAsync(request, (sender, e) =>
            {

                IRestResponse response = sender;

                MainActivity.Instance.RunOnUiThread(() =>
                {
                    //Console.WriteLine(response.Server + "\nStatusCode:" + response.StatusCode + "\nContent:" + response.Content);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {

                    }
                    else
                    {

                        if (time <= 5)
                            SendToken(token, time + 1);

                    }
                });
            });


        }

	}
}

