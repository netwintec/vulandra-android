﻿using System;
using Android.App;
using Android.Content;
using Android.Widget;
using Firebase.Messaging;

namespace Vulandra
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.MESSAGING_EVENT" })]
    public class MyFirebaseMessagingService : FirebaseMessagingService
    {
        public override void OnMessageReceived(RemoteMessage message)
        {
            // TODO(developer): Handle FCM messages here.
            // If the application is in the foreground handle both data and notification messages here.
            // Also if you intend on generating your own notifications as a result of a received FCM
            // message, here is where that should be initiated. See sendNotification method below.
            Console.WriteLine("From: " + message.From);

            string messageSt = "Nuova offerta per te";
            string title = "";
            if(message.Data.ContainsKey("message"))
            {
                messageSt = message.Data["message"];
            }

            if (message.Data.ContainsKey("title"))
            {
                title = message.Data["title"];
            }

            Console.WriteLine("Notification Data: " + title + " | " + messageSt );

			//Toast.MakeText(MainActivity.Instance.ApplicationContext, "MESSAGE RECEIVE", ToastLength.Long);

			bool close2 = false;

            try
            {
                close2 = MainActivity.Instance.IsFinishing;
            }
            catch (Exception e)
            {
                 close2 = true;
            }



				var manager =
                        (NotificationManager)this.ApplicationContext.GetSystemService(Context.NotificationService);

                Intent intent = null;

                intent = new Intent(this, typeof(SplashPage));

                var pendingIntent = PendingIntent.GetActivity(this.ApplicationContext, 0, intent, PendingIntentFlags.UpdateCurrent);

                var builder = new Notification.Builder(this.ApplicationContext)
                                                .SetDefaults(NotificationDefaults.Vibrate)//NotificationDefaults.Sound | NotificationDefaults.Vibrate)
                                                .SetSmallIcon(Resource.Mipmap.not)
                                                .SetContentTitle("Vulandra")
                                                .SetContentText(title)
                                                  .SetContentIntent(pendingIntent)
                                                  .SetAutoCancel(true);

                manager.Notify(1, builder.Build());

        }
    }
}
