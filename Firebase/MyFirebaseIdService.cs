﻿using System;
using Android.App;
using Firebase.Iid;
using RestSharp;

namespace Vulandra
{

    [Service]
    [IntentFilter(new[] { "com.google.firebase.INSTANCE_ID_EVENT" })]
    public class MyFirebaseIdService : FirebaseInstanceIdService
    {
        /**
         * Called if InstanceID token is updated. This may occur if the security of
         * the previous token had been compromised. Note that this is called when the InstanceID token
         * is initially generated so this is where you would retrieve the token.
         */
        public override void OnTokenRefresh()
        {
            // Get updated InstanceID token.
            var refreshedToken = FirebaseInstanceId.Instance.Token;

            //Console.WriteLine("FIREBASE TOKEN:" + refreshedToken);
            // TODO: Implement this method to send any registration to your app's servers.
            SendToken(refreshedToken,0);
        }

       

        public void SendToken(string token, int time)
        {
            GlobalStructureInstance GbsInstance;

            if (GlobalStructureInstance.Instance == null)
                GbsInstance = new GlobalStructureInstance();
            else
                GbsInstance = GlobalStructureInstance.Instance;
            
            var client = new RestClient(GbsInstance.BaseUrl);

			var request = new RestRequest("wp-json/apnwp/register?os_type=android&device_token=" + token+"&user_email_id="+Android.OS.Build.Serial, Method.GET);

            //Console.WriteLine(client.BaseUrl + request.Resource);

            client.ExecuteAsync(request, (sender, e) =>
            {

                IRestResponse response = sender;

                MainActivity.Instance.RunOnUiThread(() =>
                {
                    //Console.WriteLine(response.Server + "\nStatusCode:" + response.StatusCode + "\nContent:" + response.Content);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {

                    }
                    else
                    {

                        if (time <= 5)
                            SendToken(token, time + 1);

                    }
                });
            });


        }
    }
}
