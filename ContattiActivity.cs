﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Widget;
using Android.OS;
using Android.Graphics;
using Android.Views;
using Android.Content.PM;
using Android.Content;
using RestSharp;
using Newtonsoft.Json.Linq;
using System.Net.Mail;

namespace Vulandra
{
	[Activity(ScreenOrientation = ScreenOrientation.Portrait)]
	public class ContattiActivity : Activity
	{
		GlobalStructureInstance GbsInstance = GlobalStructureInstance.Instance;

		Typeface tfMB, tfOB, tfOR, tfOSB;

		RelativeLayout MenuClick, BackClick;
		ImageView MenuImg, BackImg;
		MenuView menu;

		EditText Nome, Email, Messaggio;
        LinearLayout ButtonView;

		public static ContattiActivity Instance { private set; get; }

		protected override void OnResume()
		{
			GbsInstance.SetPage(PageName.Contatti);

			base.OnResume();
		}

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			Window.RequestFeature(WindowFeatures.NoTitle);

			SetContentView(Resource.Layout.ContattiLayout);

			GbsInstance.SetPage(PageName.Contatti);

			Instance = this;

			tfMB = Typeface.CreateFromAsset(Assets, "fonts/Montserrat_Bold.ttf");
			tfOB = Typeface.CreateFromAsset(Assets, "fonts/OpenSans_Bold.ttf");
			tfOR = Typeface.CreateFromAsset(Assets, "fonts/OpenSans_Regular.ttf");
			tfOSB = Typeface.CreateFromAsset(Assets, "fonts/OpenSans_SemiBold.ttf");

			// Get our button from the layout resource,

			MenuClick = FindViewById<RelativeLayout>(Resource.Id.MenuClick);
			MenuImg = FindViewById<ImageView>(Resource.Id.MenuIcon);

			MenuClick.Click += delegate
			{
				if (menu.MenuOpen)
				{
					menu.CloseMenu();
				}
				else
				{
					menu.OpenMenu();
				}

			};

			BackClick = FindViewById<RelativeLayout>(Resource.Id.BackClick);
			BackImg = FindViewById<ImageView>(Resource.Id.BackIcon);

			BackClick.Click += delegate
			{
				if (!menu.MenuOpen)
				{
					Finish();
				}

			};


			FindViewById<TextView>(Resource.Id.ContattiTxt).Typeface = tfMB;


			//GESTIONE DATI


			FindViewById<TextView>(Resource.Id.HomeTxt).Typeface = tfOB;

			FindViewById<TextView>(Resource.Id.IndirizzoTxt).Typeface = tfOB;

			var NavigaBtn = FindViewById<LinearLayout>(Resource.Id.NavigaView);
			NavigaBtn.Click += (sender, e) =>
			{
				var geoUri = Android.Net.Uri.Parse("geo:0,0?q=44.795707,11.974674 (Vulandra Viaggi)");
				var mapIntent = new Intent(Intent.ActionView, geoUri);
				StartActivity(mapIntent);
			};

			FindViewById<TextView>(Resource.Id.NavigaTxt).Typeface = tfOB;

			FindViewById<TextView>(Resource.Id.TelefonoTxt).Typeface = tfOB;

			FindViewById<TextView>(Resource.Id.MailTxt).Typeface = tfOB;

			FindViewById<TextView>(Resource.Id.InfoTxt).Typeface = tfOSB;

			FindViewById<TextView>(Resource.Id.NomeTxt).Typeface = tfOR;

			Nome = FindViewById<EditText>(Resource.Id.NomeEditTxt);
			Nome.Typeface = tfOR;

			FindViewById<TextView>(Resource.Id.EMailTxt).Typeface = tfOR;

            Email = FindViewById<EditText>(Resource.Id.EMailEditTxt);
			Email.Typeface = tfOR;

			FindViewById<TextView>(Resource.Id.MessageTxt).Typeface = tfOR;

			Messaggio = FindViewById<EditText>(Resource.Id.MessageEditTxt);
			Messaggio.Typeface = tfOR;

			FindViewById<TextView>(Resource.Id.ButtonTxt).Typeface = tfOSB;


            ButtonView = FindViewById<LinearLayout>(Resource.Id.ButtonView);
            ButtonView.Click += (sender, e) =>
             {
                APIContatti(Nome.Text,Email.Text,Messaggio.Text);
             };

			var FBBtn = FindViewById<LinearLayout>(Resource.Id.ButtonFBView);
			FBBtn.Click += (sender, e) =>
			{
				var FBUri = Android.Net.Uri.Parse("https://www.facebook.com/vulandraviaggi/");
				var FBIntent = new Intent(Intent.ActionView, FBUri);
				StartActivity(FBIntent);
			};

			FindViewById<TextView>(Resource.Id.FBTxt).Typeface = tfOSB;

			// INIT MENU

			menu = FindViewById<MenuView>(Resource.Id.menu);
			menu.SetBackgroundColor(Color.Argb(75, 0, 0, 0));
			menu.SetMenuColor(Color.Rgb(102, 104, 162));
			menu.PAGE_TAG = "5";
			menu.CellHeight = 50;
			menu.CellWidht = 225;
			menu.MenuImg = MenuImg;

			menu.AddElement(GbsInstance.ListMenuElement);

		}

		public override void OnBackPressed()
		{
			if (menu.MenuOpen)
			{
				menu.CloseMenu();
			}
			else
			{
				Finish();

			}

			//base.OnBackPressed()
		}


        public void APIContatti(string nome,string email,string message){

            ButtonView.Clickable = false;
                      
            bool flagNome = false;
            bool flagEMail = false;
            bool flagMessage = false;

            if (string.IsNullOrEmpty(nome))
            {
                flagNome = true;
            }
            if (string.IsNullOrEmpty(message))
            {
                flagMessage = true;
            }

            if (string.IsNullOrEmpty(email))
            {
                flagEMail = true;
            }
            else
            {
                try
                {
                    MailAddress m = new MailAddress(email);
                }
                catch (FormatException)
                {
                    flagEMail = true;
                }
            }


            if (flagEMail || flagNome || flagMessage)
            {
                ButtonView.Clickable = true;
                Toast.MakeText(this, "Errore dati non inseriti o errati", ToastLength.Long).Show();
                return;

            }

            var client = new RestClient(GbsInstance.BaseUrl);

            var requestN4U = new RestRequest("mail_app.php", Method.POST);
            requestN4U.AddHeader("content-type", "application/json");
            //requestN4U.AddParameter("application/x-www-form-urlencoded", $"post={"3"}&email={"matteo.lomi@netwintec.com"}&nome={"Matteo"}&messaggio={"Test Messaggio"}", ParameterType.RequestBody);
            JObject oJsonObject = new JObject();

            oJsonObject.Add("tipo", "3");
            oJsonObject.Add("email", email);
            oJsonObject.Add("nome", nome);
            oJsonObject.Add("messaggio", message);

            requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);
            //Console.WriteLine(client.BaseUrl + "" + requestN4U.Resource+"\nJson:"+oJsonObject.ToString());

            client.ExecuteAsync(requestN4U,(response, arg2) => {
                
                //Console.WriteLine("StatusCode vulandra/mail_app.php:" + response.StatusCode +
                //                  "\nContent vulandra/mail_app.php:" + response.Content);

                if(response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    RunOnUiThread(() =>
                    {

                        ButtonView.Clickable = true;
                        Toast.MakeText(this, "Email inviata con successo", ToastLength.Long).Show();

                    });
                }
                else
                {
                    RunOnUiThread(() =>
                    {

                        ButtonView.Clickable = true;
                        Toast.MakeText(this, "Errore riprovare più tardi", ToastLength.Long).Show();

                    });
                }

            });

           
        } 

	}
}