﻿using System;
using System.Collections.Generic;
using Android.Support.V7.Widget;
using Android.Views;
using System.Linq;
using Android.Widget;
using Square.Picasso;
using Android.Graphics;
using Android.Views.Animations;
using Android.Text;
using Android.Text.Style;
using Android.Support.V4.View;
using Android.Graphics.Drawables;

namespace Vulandra
{
    public class OfferteAdapter : RecyclerView.Adapter
    {
        public static string LOGGER_CLASS = "FeedAdapter";

        // Event handler for item clicks:
        public event EventHandler<int> ItemClick;

        List<OfferteObj> ListOfferte;
        MainActivity Super;

        public int HeaderPagerCurrent = 0;

        public Typeface tfMB, tfOB, tfOR, tfOSB;

        public OfferteAdapter(List<OfferteObj> listOfferte, MainActivity super)
        {

            ListOfferte = listOfferte;
            Super = super;

            tfMB = Typeface.CreateFromAsset(Super.Assets, "fonts/Montserrat_Bold.ttf");
            tfOB = Typeface.CreateFromAsset(Super.Assets, "fonts/OpenSans_Bold.ttf");
            tfOR = Typeface.CreateFromAsset(Super.Assets, "fonts/OpenSans_Regular.ttf");
            tfOSB = Typeface.CreateFromAsset(Super.Assets, "fonts/OpenSans_SemiBold.ttf");


        }
        public override long GetItemId(int position)
        {
            return position;
        }

        public override void OnViewRecycled(Java.Lang.Object holder)
        {

        }



        // Create a new photo CardView (invoked by the layout manager): 
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            // Inflate the CardView for the photo:
            View itemView = null;

            int element = viewType / 1000;
            //Logger.Write(LOGGER_CLASS,"Element:"+element+"!"+viewType);

            itemView = LayoutInflater.From(parent.Context)
                                     .Inflate(Resource.Layout.OfferteHolderLayout, parent, false);
            OfferteHolder oh = new OfferteHolder(itemView, OnClick, 0);

            return oh;



        }

        // Fill in the contents of the photo card (invoked by the layout manager):
        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {

            OfferteHolder oh = holder as OfferteHolder;
            OfferteObj OffObj = ListOfferte[position];

            oh.CategoriaTxt.Typeface = tfOSB;
            oh.TitleTxt.Typeface = tfOSB;
            oh.DescrTxt.Typeface = tfOR;

            oh.CategoriaTxt.Text = OffObj.acf.categoria;
            oh.TitleTxt.Text = OffObj.title.rendered;
            oh.DescrTxt.Text = OffObj.acf.descrizione;

            oh.DescrTxt.Visibility = ViewStates.Visible;
            if(String.IsNullOrEmpty(OffObj.acf.descrizione))
            {
                oh.DescrTxt.Visibility = ViewStates.Gone;
            }

            Picasso.With(Super.ApplicationContext)
                   .Load(OffObj.images.medium)
                   .Into(oh.Image);
            
        }

        // Return the number of element of feedplus header plus loader on bottom
        public override int ItemCount
        {
            get { return ListOfferte.Count; }
        }

        // Raise an event when the item-click takes place:
        void OnClick(int position)
        {
            if (ListOfferte.Count > 0)
            {
                Super.ClickOnOffert(position);
            }
        }

    }

    public class OfferteHolder : RecyclerView.ViewHolder
    {
        public TextView TitleTxt { get; private set; }
        public TextView DescrTxt { get; private set; }
        public TextView CategoriaTxt { get; private set; }
        public ImageView Image { get; private set; }
        public ImageView Triangle { get; private set; }

        // Get references to the views defined in the CardView layout.
        public OfferteHolder(View itemView, Action<int> listener, int tag)
                : base(itemView)
        {


            TitleTxt = itemView.FindViewById<TextView>(Resource.Id.TitleTxt);
            DescrTxt = itemView.FindViewById<TextView>(Resource.Id.DescrTxt);
            CategoriaTxt = itemView.FindViewById<TextView>(Resource.Id.CategoriaTxt);
            Image = itemView.FindViewById<ImageView>(Resource.Id.Image);
            Triangle = itemView.FindViewById<ImageView>(Resource.Id.Triangle);

            var drawable = Triangle.Drawable;
            using (var bitmap = Bitmap.CreateBitmap(drawable.IntrinsicWidth, drawable.IntrinsicHeight, Bitmap.Config.Argb8888))
            using (var canvas = new Canvas(bitmap))
            {
                drawable.SetBounds(0, 0, canvas.Width, canvas.Height);
                drawable.Draw(canvas);

                var mat = new Matrix();
                mat.PostRotate(180);
                using (var bMapRotate = Bitmap.CreateBitmap(bitmap, 0, 0, bitmap.Width, bitmap.Height, mat, true))
                    Triangle.SetImageBitmap(bMapRotate);
            }
            Triangle.SetColorFilter(Color.White);

            itemView.Click += (sender, e) => listener(base.AdapterPosition);

        }
    }

}