﻿using System;
using System.Collections.Generic;

namespace Vulandra
{
	public class GlobalStructureInstance
	{
		//BaseUrl chiamate api
		public string BaseUrl;
		//BaseImageUrl per le immagini
		public string BaseImageUrl;
		//BaseCookie da ricercare nella chiamata di login
		public string Token;

		// BOOLEANI PER SAPERE DOVE SEI
		public bool isHome,isChiSiamo,isViaggiareSicuri;
		public bool isViaggioIdeale, isIlTuoViaggio, isContatti;


		public List<MenuElement> ListMenuElement;
		public List<string> ListDestinazioni;

		public static GlobalStructureInstance Instance { private set; get; }

		// RICHIAMABILE CON GlobalStructureInstance.Instance
		public GlobalStructureInstance()
		{

			Instance = this;

			Token = "";
			BaseUrl = "http://www.vulandraviaggi.com/";
			BaseImageUrl = "http://www.vulandraviaggi.com/wp-content/uploads/";

			ListMenuElement = new List<MenuElement>();
			ListDestinazioni = new List<string>();

			isHome = false;
			isChiSiamo = false;
			isViaggiareSicuri = false;
			isViaggioIdeale = false;
			isIlTuoViaggio = false;
			isContatti = false;

		}

		public void ClearAll()
		{

			Token = "";
			BaseUrl = "http://www.vulandraviaggi.com/";
			BaseImageUrl = "http://www.vulandraviaggi.com/wp-content/uploads/";

			ListMenuElement.Clear();
			ListDestinazioni.Clear();

			isHome = false;
			isChiSiamo = false;
			isViaggiareSicuri = false;
			isViaggioIdeale = false;
			isIlTuoViaggio = false;
			isContatti = false;

		}

		public void SetPage(string name)
		{

			isHome = false;
			isChiSiamo = false;
			isViaggiareSicuri = false;
			isViaggioIdeale = false;
			isIlTuoViaggio = false;
			isContatti = false;

			if (name == PageName.Home)
				isHome = true;

			if (name == PageName.ChiSiamo)
				isChiSiamo = true;

			if (name == PageName.ViaggiareSicuri)
				isViaggiareSicuri = true;

			if (name == PageName.ViaggioIdeale)
				isViaggioIdeale = true;

			if (name == PageName.IlTuoViaggio)
				isIlTuoViaggio = true;

			if (name == PageName.Contatti)
				isContatti = true;

		}

	}

	public static class PageName
	{

		public static string Home = "home";

		public static string ChiSiamo = "chi_siamo";

		public static string ViaggiareSicuri = "viaggiare_sicuri";

		public static string ViaggioIdeale = "viaggio_ideale";

		public static string IlTuoViaggio = "il_tuo_viaggio";

		public static string Contatti = "contatti";
	
	}
}


