﻿using System;
using Android.Content;
using Android.Graphics;
using Android.Views;
using Android.Widget;

namespace Vulandra
{
	public class SpinnerDest : ArrayAdapter<string>
	{

		public string[] List;
		IlTuoViaggioActivity Super;
		Context context;

		public SpinnerDest(IlTuoViaggioActivity super, int t, string[] s) : base(super.BaseContext, t, s)
		{
			List = s;
			Super = super;
		}

		public override View GetDropDownView(int position, Android.Views.View convertView, Android.Views.ViewGroup parent)
		{
			return getCustomDropDownView(position, convertView, parent);
		}

		public override View GetView(int position, Android.Views.View convertView, Android.Views.ViewGroup parent)
		{
			return getCustomView(position, convertView, parent);
		}

		public View getCustomDropDownView(int position, Android.Views.View convertView, Android.Views.ViewGroup parent)
		{

			var v = LayoutInflater.From(Super.ApplicationContext)
			                      .Inflate(Resource.Layout.DestinazioniSpinnerAdapter, parent, false);
			v.SetBackgroundColor(Color.ParseColor("#eeeeee"));

			var t = List[position];

			var txt = v.FindViewById<TextView>(Resource.Id.txt);
			txt.Text = t;

			var Sep = v.FindViewById<RelativeLayout>(Resource.Id.Sep);
			Sep.Visibility = ViewStates.Gone;

			var Cont = v.FindViewById<RelativeLayout>(Resource.Id.Cont);
			var lp = (RelativeLayout.LayoutParams)Cont.LayoutParameters;
			lp.Height = Super.PixelsToDp(40);
			Cont.LayoutParameters = lp;

			return v;
		}

		public View getCustomView(int position, Android.Views.View convertView, Android.Views.ViewGroup parent)
		{

			var v = LayoutInflater.From(Super.ApplicationContext)
								  .Inflate(Resource.Layout.DestinazioniSpinnerAdapter, parent, false);

			var t = List[position];

			var txt = v.FindViewById<TextView>(Resource.Id.txt);
			txt.Text = t;

			var lp1 = (RelativeLayout.LayoutParams)txt.LayoutParameters;
			lp1.LeftMargin = 0;
			txt.LayoutParameters = lp1;

			var Sep = v.FindViewById<RelativeLayout>(Resource.Id.Sep);
			Sep.Visibility = ViewStates.Visible;

			var Cont = v.FindViewById<RelativeLayout>(Resource.Id.Cont);
			var lp = (RelativeLayout.LayoutParams)Cont.LayoutParameters;
			lp.Height = Super.PixelsToDp(30);
			Cont.LayoutParameters = lp;

			return v;
		}



	}
}

