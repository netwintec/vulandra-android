﻿using System;
using Android.Graphics;
using Android.Text;
using Android.Text.Style;

namespace Vulandra
{
	public class CustomFontSpan : TypefaceSpan
	{
		private Typeface newType;

		public CustomFontSpan(String family, Typeface type) : base(family)
		{
			newType = type;
		}


		public override void UpdateDrawState(TextPaint ds)
		{
			applyCustomTypeFace(ds, newType);
		}



		public override void UpdateMeasureState(TextPaint paint)
		{
			//base.UpdateMeasureState(paint);
			applyCustomTypeFace(paint, newType);
		}

		private static void applyCustomTypeFace(Paint paint, Typeface tf)
		{
			TypefaceStyle oldStyle;
			Typeface old = paint.Typeface;
			if (old == null)
			{
				oldStyle = 0;
			}
			else
			{
				oldStyle = old.Style;
			}

			TypefaceStyle fake = oldStyle & ~tf.Style;
			////Console.WriteLine(fake.ToString());
			if ((fake & TypefaceStyle.Bold) != 0)
			{
				paint.FakeBoldText = true;
			}

			if ((fake & TypefaceStyle.Italic) != 0)
			{
				paint.TextSkewX = -0.25f;
			}

			paint.SetTypeface(tf);
		}
	}
}

