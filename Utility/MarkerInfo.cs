﻿using System;
namespace Vulandra
{
	public class MarkerInfo
	{

		public double Lat, Lon;
		public string Title, Period;
		public int IconHue;

		public MarkerInfo()
		{ }

		public MarkerInfo(double lat,double lon,string title,string period,int iconhue)
		{
			Lat = lat;
			Lon = lon;
			Title = title;
			Period = period;
			IconHue = iconhue;
		}
	}
}
