﻿using System;
namespace Vulandra
{
	public class SliderObj
	{
		public int id { get; set; }
		public string pyre_heading { get; set; }
		public string pyre_caption { get; set; }
		public string pyre_button_1 { get; set; }
		public int featured_media { get; set; }
		public MediaDetails media_details { get; set; }
	}

	public class MediaDetails
	{
		public string file { get; set; }	}
}
