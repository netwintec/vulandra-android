﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Widget;
using Android.OS;
using Android.Graphics;
using Android.Views;
using Android.Content.PM;
using Android.Content;
using Android.Animation;
using System.Net.Mail;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace Vulandra
{
	[Activity(ScreenOrientation = ScreenOrientation.Portrait)]
	public class IlTuoViaggioActivity : Activity
	{
		GlobalStructureInstance GbsInstance = GlobalStructureInstance.Instance;

		Typeface tfMB, tfOB, tfOR, tfOSB;

		RelativeLayout MenuClick, BackClick;
		ImageView MenuImg, BackImg;
		MenuView menu;

		EditText NomeEdit, CognomeEdit, EMailEdit, TelefonoEdit;

        CheckBox ContattoLIS, ContattoWH;

		Spinner ListDest;

		RelativeLayout DaView, AView;
		TextView DaDataTxt, ADataTxt;
		DateTime DaDate, ADate;
		bool DaOpen = false, AOpen = false;

		RadioButton SiRadio, NoRadio;

		RelativeLayout AutoDDView;
		ValueAnimator AutoDDOpen;
		EditText AutoDDEditTxt;

		EditText AdultiEdit,RagazziEdit,BambiniEdit,AnimaliEdit;

		CheckBox Alloggio1, Alloggio2, Alloggio3, Alloggio4;

		RelativeLayout ModalDatePicker;
		DatePicker Date;
		TextView DateBtn;

        LinearLayout ButtonSendView;

		public static IlTuoViaggioActivity Instance { private set; get; }

		protected override void OnResume()
		{
			GbsInstance.SetPage(PageName.IlTuoViaggio);

			base.OnResume();
		}

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			Window.RequestFeature(WindowFeatures.NoTitle);

			SetContentView(Resource.Layout.IlTuoViaggioLayout);

			GbsInstance.SetPage(PageName.IlTuoViaggio);

			Instance = this;

			tfMB = Typeface.CreateFromAsset(Assets, "fonts/Montserrat_Bold.ttf");
			tfOB = Typeface.CreateFromAsset(Assets, "fonts/OpenSans_Bold.ttf");
			tfOR = Typeface.CreateFromAsset(Assets, "fonts/OpenSans_Regular.ttf");
			tfOSB = Typeface.CreateFromAsset(Assets, "fonts/OpenSans_SemiBold.ttf");

			// Get our button from the layout resource,

			MenuClick = FindViewById<RelativeLayout>(Resource.Id.MenuClick);
			MenuImg = FindViewById<ImageView>(Resource.Id.MenuIcon);

			MenuClick.Click += delegate
			{
				if (menu.MenuOpen)
				{
					menu.CloseMenu();
				}
				else
				{
					menu.OpenMenu();
				}

			};

			BackClick = FindViewById<RelativeLayout>(Resource.Id.BackClick);
			BackImg = FindViewById<ImageView>(Resource.Id.BackIcon);

			BackClick.Click += delegate
			{
				if (!menu.MenuOpen)
				{
					Finish();
				}

			};


			FindViewById<TextView>(Resource.Id.IlTuoViaggioTxt).Typeface = tfMB;
			FindViewById<TextView>(Resource.Id.IlTuoViaggioDescr).Typeface = tfOSB;

			// GESTIONE DATI

			FindViewById<TextView>(Resource.Id.DatiTit).Typeface = tfOB;

			FindViewById<TextView>(Resource.Id.NomeTit).Typeface = tfOR;

			NomeEdit = FindViewById<EditText>(Resource.Id.NomeEditTxt);
			NomeEdit.Typeface = tfOR;

			FindViewById<TextView>(Resource.Id.CognomeTit).Typeface = tfOR;

			CognomeEdit = FindViewById<EditText>(Resource.Id.CognomeEditTxt);
			NomeEdit.Typeface = tfOR;

			FindViewById<TextView>(Resource.Id.EMailTit).Typeface = tfOR;

			EMailEdit = FindViewById<EditText>(Resource.Id.EMailEditTxt);
			EMailEdit.Typeface = tfOR;

			FindViewById<TextView>(Resource.Id.TelefonoTit).Typeface = tfOR;

			TelefonoEdit = FindViewById<EditText>(Resource.Id.TelefonoEditTxt);
			TelefonoEdit.Typeface = tfOR;
		
            ContattoLIS = FindViewById<CheckBox>(Resource.Id.ContattoLIS);
            ContattoLIS.Typeface = tfOR;

            ContattoWH = FindViewById<CheckBox>(Resource.Id.ContattoWH);
            ContattoWH.Typeface = tfOR;

			// GESTIONE PARTE DESTINAZIONE

			FindViewById<TextView>(Resource.Id.DestTit).Typeface = tfOB;

			ListDest = FindViewById<Spinner>(Resource.Id.Destspinner);
			var ad = new SpinnerDest(this, Resource.Layout.support_simple_spinner_dropdown_item, GbsInstance.ListDestinazioni.ToArray());
			ListDest.Adapter = ad;

			// GESTIONE PARTE WHEN

			FindViewById<TextView>(Resource.Id.WhenTit).Typeface = tfOB;

			FindViewById<TextView>(Resource.Id.DaTxt).Typeface = tfOR;
			FindViewById<TextView>(Resource.Id.ATxt).Typeface = tfOR;

			DaView = FindViewById<RelativeLayout>(Resource.Id.DaView);
			DaView.Click += (sender, e) =>
			{
				DaOpen = true;
				AOpen = false;

				ModalDatePicker.Visibility = ViewStates.Visible;

				Date.UpdateDate(DaDate.Year, DaDate.Month - 1, DaDate.Day);
				Date.MinDate = 0;
				Date.MinDate = FromDateTime(DateTime.Now.AddDays(0)) * 1000;
			};

			AView = FindViewById<RelativeLayout>(Resource.Id.AView);
			AView.Click += (sender, e) =>
			{
				DaOpen = false;
				AOpen = true;

				ModalDatePicker.Visibility = ViewStates.Visible;

				Date.UpdateDate(ADate.Year, ADate.Month - 1, ADate.Day);
				Date.MinDate = 0;
				Date.MinDate = FromDateTime(DaDate.AddDays(+1)) * 1000;
			};

			DaDataTxt = FindViewById<TextView>(Resource.Id.DaDataTxt);
			DaDataTxt.Typeface = tfOB;

			ADataTxt = FindViewById<TextView>(Resource.Id.ADataTxt);
			ADataTxt.Typeface = tfOB;

			// GESTIONE DATE

			DateTime now = DateTime.Now;

			DaDate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);
			ADate = DaDate.AddDays(1);

			DaDataTxt.Text = FormatDateTime(DaDate);
			ADataTxt.Text = FormatDateTime(ADate);

			// GESTIONE AUTO

			FindViewById<TextView>(Resource.Id.AutoTit).Typeface = tfOB;

			SiRadio = FindViewById<RadioButton>(Resource.Id.SiRadio);
			SiRadio.Typeface = tfOR;

			NoRadio = FindViewById<RadioButton>(Resource.Id.NoRadio);
			NoRadio.Typeface = tfOR;
			NoRadio.Checked = true;

			SiRadio.CheckedChange += (s, e) =>
			{

				if (e.IsChecked)
				{
					SiRadio.Enabled = false;
					NoRadio.Enabled = false;

					//AutoDDView.Visibility = ViewStates.Visible;
					AutoDDOpen.SetIntValues(0, 100);
					AutoDDOpen.Start();

				}
				else
				{
					SiRadio.Enabled = false;
					NoRadio.Enabled = false;
					//AutoDDView.Visibility = ViewStates.Gone;
					AutoDDOpen.SetIntValues(100, 0);
					AutoDDOpen.Start();
					AutoDDEditTxt.Text = "";

				}
			};

			// GESTIONE DAY AUTO

			AutoDDView = FindViewById<RelativeLayout>(Resource.Id.AutoDDView);
			var lp = (RelativeLayout.LayoutParams)AutoDDView.LayoutParameters;
			lp.Height = 0;
			AutoDDView.LayoutParameters = lp;

			FindViewById<TextView>(Resource.Id.AutoDDTit).Typeface = tfOR;

			AutoDDEditTxt = FindViewById<EditText>(Resource.Id.AutoDDEditTxt);
			AutoDDEditTxt.Typeface = tfOR;

			AutoDDOpen = ValueAnimator.OfInt(0);
			AutoDDOpen.SetDuration(300);
			AutoDDOpen.Update += (sender, e) =>
			{
				int val = (int)e.Animation.AnimatedValue;

				var lp1 = (RelativeLayout.LayoutParams)AutoDDView.LayoutParameters;
				lp1.Height = (int)(PixelsToDp(35) * (val / 100f));
				AutoDDView.LayoutParameters = lp1;

			};
			AutoDDOpen.AnimationEnd += (sender, e) =>
			{
				SiRadio.Enabled = true;
				NoRadio.Enabled = true;
			};

			// GESTIONE PERSONE

			FindViewById<TextView>(Resource.Id.ViaggiatoriTit).Typeface = tfOB;

			FindViewById<TextView>(Resource.Id.AdultiTit).Typeface = tfOR;

			AdultiEdit = FindViewById<EditText>(Resource.Id.AdultiEditTxt);
			AdultiEdit.Typeface = tfOR;

			FindViewById<TextView>(Resource.Id.RagazziTit).Typeface = tfOR;

			RagazziEdit = FindViewById<EditText>(Resource.Id.RagazziEditTxt);
			RagazziEdit.Typeface = tfOR;

			FindViewById<TextView>(Resource.Id.BambiniTit).Typeface = tfOR;

			BambiniEdit = FindViewById<EditText>(Resource.Id.BambiniEditTxt);
			BambiniEdit.Typeface = tfOR;

			FindViewById<TextView>(Resource.Id.AnimaliTit).Typeface = tfOR;

			AnimaliEdit = FindViewById<EditText>(Resource.Id.AnimaliEditTxt);
			AnimaliEdit.Typeface = tfOR;
		
			// GSTIONE ALLOGGIO

			FindViewById<TextView>(Resource.Id.AlloggioTit).Typeface = tfOB;

			Alloggio1 = FindViewById<CheckBox>(Resource.Id.Alloggio1);
			Alloggio1.Typeface = tfOR;

			Alloggio2 = FindViewById<CheckBox>(Resource.Id.Alloggio2);
			Alloggio2.Typeface = tfOR;

			Alloggio3 = FindViewById<CheckBox>(Resource.Id.Alloggio3);
			Alloggio3.Typeface = tfOR;

			Alloggio4 = FindViewById<CheckBox>(Resource.Id.Alloggio4);
			Alloggio4.Typeface = tfOR;

			// GESTIONE MODALE PICKER

			ModalDatePicker = FindViewById<RelativeLayout>(Resource.Id.ModalDatePicker);
			ModalDatePicker.Visibility = ViewStates.Gone;
			ModalDatePicker.Clickable = true;

			Date = FindViewById<DatePicker>(Resource.Id.Date);

			DateBtn = FindViewById<TextView>(Resource.Id.DateBtn);
			DateBtn.Typeface = tfOSB;
			DateBtn.Click += (sender, e) =>
			{
				ModalDatePicker.Visibility = ViewStates.Gone;

				if (DaOpen)
				{
					DaDate = new DateTime(Date.Year, Date.Month + 1, Date.DayOfMonth, 0, 0, 0);
					DaDataTxt.Text = FormatDateTime(DaDate);

					if (ADate.CompareTo(DaDate) < 0)
					{
						ADate = DaDate.AddDays(1);
						ADataTxt.Text = FormatDateTime(ADate);
					}
				}

				if (AOpen)
				{
					ADate = new DateTime(Date.Year, Date.Month + 1, Date.DayOfMonth, 0, 0, 0);
					ADataTxt.Text = FormatDateTime(ADate);
				}


				DaOpen = false;
				AOpen = false;
			};

            ButtonSendView = FindViewById<LinearLayout>(Resource.Id.ButtonSendView);
            ButtonSendView.Click += (sender, e) =>
            {

                string dest = GbsInstance.ListDestinazioni[ListDest.SelectedItemPosition];

                string tipo_alloggio = "";
                if (Alloggio1.Checked)
                    tipo_alloggio += "Casa,";
                if (Alloggio2.Checked)
                    tipo_alloggio += "Residence,";
                if (Alloggio3.Checked)
                    tipo_alloggio += "Hotel,";
                if (Alloggio4.Checked)
                    tipo_alloggio += "Villaggio Turistico,";

                if (!string.IsNullOrEmpty(tipo_alloggio))
                    tipo_alloggio=tipo_alloggio.Substring(0, tipo_alloggio.Length - 1);
                
                APITuoViaggio(NomeEdit.Text, CognomeEdit.Text, EMailEdit.Text, TelefonoEdit.Text,
                              ContattoLIS.Checked,ContattoWH.Checked,
                              dest, DaDataTxt.Text, ADataTxt.Text, SiRadio.Checked, AutoDDEditTxt.Text,
                              AdultiEdit.Text, BambiniEdit.Text, RagazziEdit.Text, AnimaliEdit.Text, tipo_alloggio);
            };

			// INIT MENU

			menu = FindViewById<MenuView>(Resource.Id.menu);
			menu.SetBackgroundColor(Color.Argb(75, 0, 0, 0));
			menu.SetMenuColor(Color.Rgb(102, 104, 162));
			menu.PAGE_TAG = "4";
			menu.CellHeight = 50;
			menu.CellWidht = 225;
			menu.MenuImg = MenuImg;

			menu.AddElement(GbsInstance.ListMenuElement);

		}

        //nome,cognome,email,telefono,destinazione,data_partenza,data_fine,autonoleggio,giorni_autonoleggio,adulti,bambini,ragazzi,animali,tipo_alloggio.
        public void APITuoViaggio(string nome, string cognome, string email, string telefono,
                                  bool lis,bool whatsapp,
                                  string destinazione, string data_partenza, string data_fine,
                                  bool autonoleggio, string giorni_autonoleggio, string adulti,
                                  string bambini, string ragazzi, string animali,
                                  string tipo_alloggio)
        {

            ButtonSendView.Clickable = false;

            bool flagNome = false;
            bool flagEMail = false;
            bool flagCognome = false;
            bool flagNoleggio = false;
            bool flagViaggiatori = false;
            bool flagAlloggio = false;


            if (string.IsNullOrEmpty(nome))
            {
                flagNome = true;
            }
            if (string.IsNullOrEmpty(cognome))
            {
                flagCognome = true;
            }
            if (autonoleggio && string.IsNullOrEmpty(nome))
            {
                flagNoleggio = true;
            }
            if (string.IsNullOrEmpty(adulti+bambini+ragazzi+animali))
            {
                flagViaggiatori = true;
            }
            if (string.IsNullOrEmpty(tipo_alloggio))
            {
                flagAlloggio = true;
            }

            if (string.IsNullOrEmpty(email))
            {
                flagEMail = true;
            }
            else
            {
                try
                {
                    MailAddress m = new MailAddress(email);
                }
                catch (FormatException)
                {
                    flagEMail = true;
                }
            }

            if (flagEMail || flagNome || flagCognome || flagNoleggio || flagViaggiatori || flagAlloggio)
            {
                ButtonSendView.Clickable = true;
                Toast.MakeText(this, "Errore dati non inseriti o errati", ToastLength.Long).Show();
                return;

            }

            var client = new RestClient(GbsInstance.BaseUrl);

            var requestN4U = new RestRequest("mail_app.php", Method.POST);
            requestN4U.AddHeader("content-type", "application/json");
            //requestN4U.AddParameter("application/x-www-form-urlencoded", $"post={"3"}&email={"matteo.lomi@netwintec.com"}&nome={"Matteo"}&messaggio={"Test Messaggio"}", ParameterType.RequestBody);
            JObject oJsonObject = new JObject();

            //nome,cognome,email,telefono,destinazione,data_partenza,data_fine,
            //autonoleggio,giorni_autonoleggio,adulti,bambini,ragazzi,animali,tipo_alloggio.


            oJsonObject.Add("tipo", "2");
            oJsonObject.Add("nome", nome);
            oJsonObject.Add("cognome", cognome);
            oJsonObject.Add("email", email);
            oJsonObject.Add("telefono", telefono);
            oJsonObject.Add("videocallinlis", lis.ToString());
            oJsonObject.Add("whatsapp", whatsapp.ToString());
            oJsonObject.Add("destinazione", destinazione);
            oJsonObject.Add("data_partenza", data_partenza);
            oJsonObject.Add("data_fine", data_fine);
            oJsonObject.Add("autonoleggio", autonoleggio.ToString());
            oJsonObject.Add("giorni_autonoleggio", giorni_autonoleggio);
            oJsonObject.Add("adulti", adulti);
            oJsonObject.Add("bambini", bambini);
            oJsonObject.Add("ragazzi", ragazzi);
            oJsonObject.Add("animali", animali);
            oJsonObject.Add("tipo_alloggio", tipo_alloggio);

            requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);
            Console.WriteLine(client.BaseUrl + "" + requestN4U.Resource);

            client.ExecuteAsync(requestN4U, (response, arg2) => {

                Console.WriteLine("StatusCode vulandra/mail_app.php:" + response.StatusCode +
                                  "\nContent vulandra/mail_app.php:" + response.Content);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    RunOnUiThread(() =>
                    {

                        ButtonSendView.Clickable = true;
                        Toast.MakeText(this, "Email inviata con successo", ToastLength.Long).Show();

                    });
                }
                else
                {
                    RunOnUiThread(() =>
                    {

                        ButtonSendView.Clickable = true;
                        Toast.MakeText(this, "Errore riprovare più tardi", ToastLength.Long).Show();

                    });
                }

            });


        }

    

		public override void OnBackPressed()
		{
			if (menu.MenuOpen)
			{
				menu.CloseMenu();
			}
			else
			{
				Finish();

			}

			//base.OnBackPressed()
		}

		public string FormatDateTime(DateTime dt)
		{

			string formatted = "";

			formatted = dt.ToString("dd MMM yyyy");

			return formatted;

		}

		public long FromDateTime(DateTime time)
		{

			DateTime local = new DateTime(time.Year, time.Month, time.Day,
										  time.Hour, time.Minute, time.Second,
										  DateTimeKind.Local);
			DateTime utc = local.ToUniversalTime();

			var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

			long sec = (long)(utc - epoch).TotalSeconds;

			return sec;
		}

		public int PixelsToDp(int pixels)
		{
			return (int)Android.Util.TypedValue.ApplyDimension(Android.Util.ComplexUnitType.Dip, pixels, ApplicationContext.Resources.DisplayMetrics);
		}


	}
}
