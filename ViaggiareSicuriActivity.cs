﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Widget;
using Android.OS;
using Android.Graphics;
using Android.Views;
using Android.Content.PM;
using Android.Content;
using Android.Webkit;

namespace Vulandra
{
	[Activity(ScreenOrientation = ScreenOrientation.Portrait)]
	public class ViaggiareSicuriActivity : Activity
	{
		GlobalStructureInstance GbsInstance = GlobalStructureInstance.Instance;

		Typeface tfMB, tfOB, tfOR, tfOSB;

		RelativeLayout MenuClick, BackClick;
		ImageView MenuImg, BackImg;
		MenuView menu;

		WebView Web;
		ProgressBar Loading;

		public static ViaggiareSicuriActivity Instance { private set; get; }

		protected override void OnResume()
		{
			GbsInstance.SetPage(PageName.ViaggiareSicuri);

			base.OnResume();
		}

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			Window.RequestFeature(WindowFeatures.NoTitle);

			SetContentView(Resource.Layout.ViaggiareSicuriLayout);

			GbsInstance.SetPage(PageName.ViaggiareSicuri);

			Instance = this;

			tfMB = Typeface.CreateFromAsset(Assets, "fonts/Montserrat_Bold.ttf");
			tfOB = Typeface.CreateFromAsset(Assets, "fonts/OpenSans_Bold.ttf");
			tfOR = Typeface.CreateFromAsset(Assets, "fonts/OpenSans_Regular.ttf");
			tfOSB = Typeface.CreateFromAsset(Assets, "fonts/OpenSans_SemiBold.ttf");

			// Get our button from the layout resource,

			MenuClick = FindViewById<RelativeLayout>(Resource.Id.MenuClick);
			MenuImg = FindViewById<ImageView>(Resource.Id.MenuIcon);

			MenuClick.Click += delegate
			{
				if (menu.MenuOpen)
				{
					menu.CloseMenu();
				}
				else
				{
					menu.OpenMenu();
				}

			};

			BackClick = FindViewById<RelativeLayout>(Resource.Id.BackClick);
			BackImg = FindViewById<ImageView>(Resource.Id.BackIcon);

			BackClick.Click += delegate
			{
				if (!menu.MenuOpen)
				{
					Finish();
				}

			};


			Web = FindViewById<WebView>(Resource.Id.ViaggiareSicuriWeb);
			Loading = FindViewById<ProgressBar>(Resource.Id.Loading);

			Web.Visibility = ViewStates.Gone;

			Web.Settings.JavaScriptEnabled = true;
			Web.Settings.PluginsEnabled = true;
			Web.LoadUrl("http://www.viaggiaresicuri.it/home/");

			Web.SetWebViewClient(new MyWebViewClientHome());
			Web.SetWebChromeClient(new MyWebViewChromeHome());

			Loading.IndeterminateDrawable.SetColorFilter(Color.Rgb(44, 169, 91), PorterDuff.Mode.Multiply);

			// INIT MENU

			menu = FindViewById<MenuView>(Resource.Id.menu);
			menu.SetBackgroundColor(Color.Argb(75, 0, 0, 0));
			menu.SetMenuColor(Color.Rgb(102, 104, 162));
			menu.PAGE_TAG = "2";
			menu.CellHeight = 50;
			menu.CellWidht = 225;
			menu.MenuImg = MenuImg;

			menu.AddElement(GbsInstance.ListMenuElement);

		}

		public override void OnBackPressed()
		{
			if (menu.MenuOpen)
			{
				menu.CloseMenu();
			}
			else
			{
				Finish();

			}

			//base.OnBackPresse)
		}

	}

	class MyWebViewChromeHome : WebChromeClient
	{
		public MyWebViewChromeHome()
		{
		}
		public override void OnProgressChanged(WebView view, int progress)
		{
			if (progress == 100)
			{
				view.Visibility = ViewStates.Visible;
			}
		}
	}

	class MyWebViewClientHome : WebViewClient
	{
		Boolean loadingFinished = true;
		Boolean redirect = false;

		public MyWebViewClientHome()
		{
		}

		public override bool ShouldOverrideUrlLoading(WebView view, string url)
		{

			//Console.WriteLine("URLLLL 1:"+url);

			return true;
		}

		public override void OnPageStarted(WebView view, string url, Android.Graphics.Bitmap favicon)
		{
			base.OnPageStarted(view, url, favicon);
			loadingFinished = false;
			//System.Console.WriteLine("Start Loading!!!");
		}

		public override void OnPageFinished(WebView view, string url)
		{
			if (!redirect)
			{
				//System.//Console.WriteLine("1");
				//loadingFinished = true;
			}

			if (loadingFinished && !redirect)
			{
				//System.//Console.WriteLine("Finished Loading!!!");
				//RelativeLayout.LayoutParams par = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.MatchParent);
				//par.SetMargins(0, 0, 0, 0);
				//view.LayoutParameters = par;//new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent,LinearLayout.LayoutParams.MatchParent);
			}
			else
			{
				//System.//Console.WriteLine("2");
				//redirect = false;
			}
		}

	}


}
