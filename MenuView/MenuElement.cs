﻿using System;
using Android.Graphics;

namespace Vulandra
{
	public class MenuElement
	{
		public string Text;
		public int ImgResource;
		public Color TextColor;
		public Typeface TextFont;
		public string TAG;
		public EventHandler Click;

		public MenuElement()
		{
			
		}

		public MenuElement(string text, int resource, string tag)
		{
			Text = text;
			ImgResource = resource;
			TAG = tag;
		}

		public MenuElement(string text, Color textColor,Typeface textFont,int resource, string tag)
		{
			Text = text;
			TextColor = textColor;
			TextFont = textFont;
			ImgResource = resource;
			TAG = tag;
		}
	}
}
