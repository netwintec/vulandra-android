﻿using System;
using Android.Content;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Animation;
using System.Collections.Generic;

namespace Vulandra
{
	public class MenuView : RelativeLayout
	{

		Context context;
		public string PAGE_TAG;

		public RelativeLayout Background;
		Color BackgroundColor;

		public LinearLayout Menu;
		Color MenuColor;
		bool IsRight;

		public int CellHeight;
		public int CellWidht;

		ValueAnimator MenuValueAnimator;
		public bool MenuOpen { get; private set; }
		public ImageView MenuImg;

		List<MenuElement> ListElement= new List<MenuElement>();


		public MenuView(Context c) : base (c)
		{
			context = c;
			InitComponent();
		}

		public MenuView(Context c, Android.Util.IAttributeSet a) : base (c,a)
		{
			context = c;
            InitComponent();
		}

		public void InitComponent()
		{
			BackgroundColor = Color.White;
			MenuColor = Color.Blue;
			IsRight = false;
			MenuOpen = false;
			CellHeight = 50;
			CellWidht = 200;

            this.Visibility = ViewStates.Gone;

			Background = new RelativeLayout(context);
			Background.SetBackgroundColor(BackgroundColor);
			AddView(Background);
			Background.LayoutParameters = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.MatchParent);
			Background.Click += (sender, e) =>
			{
				CloseMenu();
			};


			ScrollView ScrollMenu = new ScrollView(context);
			ScrollMenu.VerticalScrollBarEnabled = false;
			ScrollMenu.HorizontalScrollBarEnabled = false;
			Background.AddView(ScrollMenu);
			ScrollMenu.LayoutParameters = new RelativeLayout.LayoutParams(0,0);

			Menu = new LinearLayout(context);
			Menu.Orientation = Orientation.Vertical;
			Menu.SetBackgroundColor(MenuColor);
			Menu.SetPadding(0,PixelsToDp(5),0,PixelsToDp(15));
			ScrollMenu.AddView(Menu);
			Menu.LayoutParameters = new ScrollView.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.MatchParent);


			MenuValueAnimator = ValueAnimator.OfInt(0);
			MenuValueAnimator.SetDuration(300);
			MenuValueAnimator.Update += (sender, e) =>
			{

				RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)ScrollMenu.LayoutParameters;
				layoutParams.Height = PixelsToDp(((Menu.ChildCount * CellHeight) + 20) * ((float)e.Animation.AnimatedValue / 100f));
				layoutParams.Width = PixelsToDp(CellWidht * ((float)e.Animation.AnimatedValue / 100f));
				ScrollMenu.LayoutParameters = layoutParams;

			};
			MenuValueAnimator.AnimationEnd += (sender, e) =>
			{

				if (!MenuOpen)
				{
					if(MenuImg != null)
						MenuImg.SetImageResource(Resource.Drawable.MenuClose);
					
					this.Visibility = ViewStates.Gone;
					ScrollMenu.ScrollTo(0, 0);
				}
			};


		}

		public void AddElement(MenuElement me)
		{

            CreateView(me);

		}

		public void AddElement(List<MenuElement> ListMe)
		{
			foreach (var me in ListMe)
			{
				CreateView(me);
			}
		}

		private void CreateView(MenuElement me)
		{
			RelativeLayout rl = new RelativeLayout(context);
			rl.SetBackgroundColor(Color.Transparent);
			Menu.AddView(rl);
			rl.LayoutParameters = new LinearLayout.LayoutParams(PixelsToDp(CellWidht), PixelsToDp(CellHeight));


			int ImgDim = CellHeight / 2;

			ImageView img = new ImageView(context);
			img.SetImageResource(me.ImgResource);
			img.SetAdjustViewBounds(true);
			rl.AddView(img);
			var lpImg = new RelativeLayout.LayoutParams(PixelsToDp(ImgDim), PixelsToDp(ImgDim));
			lpImg.LeftMargin = PixelsToDp(15);
			lpImg.AddRule(LayoutRules.CenterVertical);
			img.LayoutParameters = lpImg;


			TextView txt = new TextView(context);
			txt.Text = me.Text;
			txt.SetMaxLines(2);
			if (me.TextFont != null)
				txt.Typeface = me.TextFont;

			if (me.TextColor != null)
				txt.SetTextColor(me.TextColor);
			else
				txt.SetTextColor(Color.Black);
			rl.AddView(txt);
			var lptxt = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.WrapContent);
			lptxt.RightMargin = PixelsToDp(15);
			lptxt.LeftMargin = PixelsToDp(15 + ImgDim + 10);
			lptxt.AddRule(LayoutRules.CenterVertical);
			//lptxt.AddRule(LayoutRules.RightOf,img.Id);
			txt.LayoutParameters = lptxt;


			RelativeLayout sep = new RelativeLayout(context);
			sep.SetBackgroundColor(Color.White);
			rl.AddView(sep);
			var lpsep = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, PixelsToDp(1));
			lpsep.AddRule(LayoutRules.AlignParentBottom);
			sep.LayoutParameters = lpsep;


			rl.Click += delegate
			{
				if (me.TAG == PAGE_TAG)
				{
					CloseMenu();
				}
				else
				{
					if (me.Click != null)
						me.Click(this, new EventArgs());

					CloseMenu();
				}
			};

			ListElement.Add(me);
		}

		public void OpenMenu() {

			if (!MenuValueAnimator.IsRunning && !MenuOpen)
			{
				this.Visibility = ViewStates.Visible;

				if(MenuImg != null)
					MenuImg.SetImageResource(Resource.Drawable.MenuOpen);

				MenuOpen = true;
				MenuValueAnimator.SetIntValues(0, 100);
				MenuValueAnimator.Start();
			}
		}

		public void CloseMenu()
		{

			if (!MenuValueAnimator.IsRunning && MenuOpen)
			{
				MenuOpen = false;
				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

			}
		}




		public void SetRightMenu(bool isRight)
		{
			IsRight = isRight;

			if (IsRight)
			{
				var lp = (RelativeLayout.LayoutParams)Menu.LayoutParameters;
				lp.AddRule(LayoutRules.AlignParentRight);
				Menu.LayoutParameters = lp;
			}
			else
			{
				var lp = (RelativeLayout.LayoutParams)Menu.LayoutParameters;
				try
				{
					lp.RemoveRule(LayoutRules.AlignParentRight);
				}
				catch (Exception e) { }
				Menu.LayoutParameters = lp;
			}
		}

		public void SetBackgroundColor(Color c)
		{
			BackgroundColor = c;
			Background.SetBackgroundColor(BackgroundColor);
		}

		public void SetMenuColor(Color c)
		{
			MenuColor = c;
			Menu.SetBackgroundColor(MenuColor);
		}

		private int PixelsToDp(float pixels)
		{
			return (int)Android.Util.TypedValue.ApplyDimension(Android.Util.ComplexUnitType.Dip, pixels, Resources.DisplayMetrics);
		}
	}
}

