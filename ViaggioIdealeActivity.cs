﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Widget;
using Android.OS;
using Android.Graphics;
using Android.Views;
using Android.Content.PM;
using Android.Content;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using RestSharp;
using Newtonsoft.Json.Linq;
using System.Net.Mail;

namespace Vulandra
{
	[Activity(ScreenOrientation = ScreenOrientation.Portrait)]
	public class ViaggioIdealeActivity : Activity, IOnMapReadyCallback
	{
		GlobalStructureInstance GbsInstance = GlobalStructureInstance.Instance;

		Typeface tfMB, tfOB, tfOR, tfOSB;

		RelativeLayout MenuClick, BackClick;
		ImageView MenuImg, BackImg;
		MenuView menu;

		RelativeLayout DaView, AView;
		TextView DaDataTxt, ADataTxt;
		DateTime DaDate, ADate;
		bool DaOpen = false, AOpen = false;

		MapFragment mapFrag;
		GoogleMap gMap;

		public Dictionary<string,MarkerInfo> MarkerInfoDictionary = new Dictionary<string,MarkerInfo>();
		public List<Marker> MarkerList = new List<Marker>();
		int HueGiallo = 49, HueViola = 290, HueRosso = 352, HueVerde = 141, HueArancione = 19, HueBlu = 228;

		RelativeLayout ModalPreventivo;

		RelativeLayout ModalDatePicker;
		DatePicker Date;
		TextView DateBtn;

        EditText NomeEditTxt, CognomeEditTxt, EMailEditTxt, TelefonoEditTxt;
        string ModalMarkerTitle;

		LinearLayout SendBtn;

		public static ViaggioIdealeActivity Instance { private set; get; }

		protected override void OnResume()
		{
			GbsInstance.SetPage(PageName.ViaggioIdeale);

			base.OnResume();
		}

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			Window.RequestFeature(WindowFeatures.NoTitle);

			SetContentView(Resource.Layout.ViaggioIdealeLayout);

			GbsInstance.SetPage(PageName.ViaggioIdeale);

			Instance = this;

			tfMB = Typeface.CreateFromAsset(Assets, "fonts/Montserrat_Bold.ttf");
			tfOB = Typeface.CreateFromAsset(Assets, "fonts/OpenSans_Bold.ttf");
			tfOR = Typeface.CreateFromAsset(Assets, "fonts/OpenSans_Regular.ttf");
			tfOSB = Typeface.CreateFromAsset(Assets, "fonts/OpenSans_SemiBold.ttf");

			// Get our button from the layout resource,

			MenuClick = FindViewById<RelativeLayout>(Resource.Id.MenuClick);
			MenuImg = FindViewById<ImageView>(Resource.Id.MenuIcon);

			MenuClick.Click += delegate
			{
				if (menu.MenuOpen)
				{
					menu.CloseMenu();
				}
				else
				{
					menu.OpenMenu();
				}

			};

			BackClick = FindViewById<RelativeLayout>(Resource.Id.BackClick);
			BackImg = FindViewById<ImageView>(Resource.Id.BackIcon);

			BackClick.Click += delegate
			{
				if (!menu.MenuOpen)
				{
					Finish();
				}

			};


			FindViewById<TextView>(Resource.Id.ViaggioIdealeTxt).Typeface = tfMB;
			FindViewById<TextView>(Resource.Id.ViaggioIdealeDescr).Typeface = tfOSB;


			FindViewById<TextView>(Resource.Id.DaTxt).Typeface = tfOR;
			FindViewById<TextView>(Resource.Id.ATxt).Typeface = tfOR;

			DaView = FindViewById<RelativeLayout>(Resource.Id.DaView);
			DaView.Click += (sender, e) =>
			{
				DaOpen = true;
				AOpen = false;

				ModalDatePicker.Visibility = ViewStates.Visible;

				Date.UpdateDate(DaDate.Year, DaDate.Month - 1, DaDate.Day);
				Date.MinDate = 0;
				Date.MinDate = FromDateTime(DateTime.Now.AddDays(0)) * 1000;
			};

			AView = FindViewById<RelativeLayout>(Resource.Id.AView);
			AView.Click += (sender, e) =>
			{
				DaOpen = false;
				AOpen = true;

				ModalDatePicker.Visibility = ViewStates.Visible;

				Date.UpdateDate(ADate.Year, ADate.Month - 1, ADate.Day);
				Date.MinDate = 0;
				Date.MinDate = FromDateTime(DaDate.AddDays(+1)) * 1000;
			};

			DaDataTxt = FindViewById<TextView>(Resource.Id.DaDataTxt);
			DaDataTxt.Typeface = tfOB;

			ADataTxt = FindViewById<TextView>(Resource.Id.ADataTxt);
			ADataTxt.Typeface = tfOB;

			// GESTIONE DATE

			DateTime now = DateTime.Now;

			DaDate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);
			ADate = DaDate.AddDays(1);

			DaDataTxt.Text = FormatDateTime(DaDate);
			ADataTxt.Text = FormatDateTime(ADate);


			// INIZIALIZZAZIONE MARKER

			// ARANCIONE
			MarkerInfoDictionary.Add("m0", new MarkerInfo(59.948807, -109.602792, "CANADA", "maggio - settembre", HueArancione));
			MarkerInfoDictionary.Add("m1", new MarkerInfo(40.713390, -73.990562, "NEW YORK", "evergreen", HueArancione));
			MarkerInfoDictionary.Add("m2", new MarkerInfo(35.932960, -120.891827, "LOS ANGELES E SAN FRANCISCO", "aprile - settembre", HueArancione));
			MarkerInfoDictionary.Add("m3", new MarkerInfo(25.788992, -80.205249, "MIAMI", "novembre - aprile", HueArancione));
			MarkerInfoDictionary.Add("m4", new MarkerInfo(20.486772, -157.244795, "HAWAII", "maggio - settembre", HueArancione));

			// ROSSO
			MarkerInfoDictionary.Add("m5", new MarkerInfo(21.659385, -78.105960, "CUBA", "novembre - aprile", HueRosso));
			MarkerInfoDictionary.Add("m6", new MarkerInfo(23.696244, -102.821168, "MESSICO", "novembre - aprile", HueRosso));
			MarkerInfoDictionary.Add("m7", new MarkerInfo(18.538669, -69.910145, "SANTO DOMINGO", "novembre - aprile", HueRosso));
			MarkerInfoDictionary.Add("m8", new MarkerInfo(11.343386, -61.714891, "ANTILLE", "dicembre - aprile", HueRosso));
			MarkerInfoDictionary.Add("m9", new MarkerInfo(11.843107, -66.736363, "LOS ROQUES\n(Venezuela)", "dicembre - aprile", HueRosso));
			MarkerInfoDictionary.Add("m10", new MarkerInfo(-9.309048, -53.196412, "BRASILE", "ottobre - marzo", HueRosso));
			MarkerInfoDictionary.Add("m11", new MarkerInfo(-36.058717, -65.501099, "ARGENTINA", "novembre - aprile", HueRosso));

			// VERDE
			MarkerInfoDictionary.Add("m12", new MarkerInfo(64.820631, -18.494737, "ISLANDA", "giugno - settembre", HueVerde));
			MarkerInfoDictionary.Add("m13", new MarkerInfo(65.543632, 23.318341, "EUROPA DEL NORD", "giugno - settembre", HueVerde));
			MarkerInfoDictionary.Add("m14", new MarkerInfo(42.511424, 12.512715, "MEDITERRANEO", "maggio - ottobre", HueVerde));

			// GIALLO
			MarkerInfoDictionary.Add("m15", new MarkerInfo(32.210394, -6.749489, "MAROCCO", "aprile - ottobre", HueGiallo));
			MarkerInfoDictionary.Add("m16", new MarkerInfo(33.656139, 9.537253, "TUNISIA", "aprile - ottobre", HueGiallo));
			MarkerInfoDictionary.Add("m17", new MarkerInfo(15.729612, -24.192744, "CAPO VERDE", "novembre - luglio", HueGiallo));
			MarkerInfoDictionary.Add("m18", new MarkerInfo(26.218508, 28.861057, "EGITTO", "aprile - ottobre", HueGiallo));
			MarkerInfoDictionary.Add("m19", new MarkerInfo(0.472157, 36.742038, "KENYA", "dicembre - marzo", HueGiallo));
			MarkerInfoDictionary.Add("m20", new MarkerInfo(-6.195318, 37.072609, "ZANZIBAR E TANZANIA", "dicembre - marzo", HueGiallo));
			MarkerInfoDictionary.Add("m21", new MarkerInfo(-4.673900, 55.453885, "SEYCHELLES", "aprile - ottobre", HueGiallo));
			MarkerInfoDictionary.Add("m22", new MarkerInfo(-20.264646, 57.592525, "MAURITIUS", "aprile - ottobre", HueGiallo));
			MarkerInfoDictionary.Add("m23", new MarkerInfo(-31.330209, 24.016350, "SUD AFRICA", "ottobre - aprile", HueGiallo));
			MarkerInfoDictionary.Add("m24", new MarkerInfo(-20.599683, 46.100124, "MADAGASCAR", "aprile - ottobre", HueGiallo));

			// BLU
			MarkerInfoDictionary.Add("m25", new MarkerInfo(63.322945, 92.993800, "RUSSIA", "maggio - settembre", HueBlu));
			MarkerInfoDictionary.Add("m26", new MarkerInfo(36.782442, 138.925245, "GIAPPONE", "dicembre - aprile", HueBlu));
			MarkerInfoDictionary.Add("m27", new MarkerInfo(35.417176, 104.361449, "CINA", "aprile - settembre", HueBlu));
			MarkerInfoDictionary.Add("m28", new MarkerInfo(25.102363, 55.153171, "DUBAI", "novembre - maggio", HueBlu));
			MarkerInfoDictionary.Add("m29", new MarkerInfo(21.815329, 96.689845, "BIRMANIA", "novembre - marzo", HueBlu));
			MarkerInfoDictionary.Add("m30", new MarkerInfo(17.327963, 74.988953, "INDIA E LACCADIVE", "novembre - aprile", HueBlu));
			MarkerInfoDictionary.Add("m31", new MarkerInfo(-2.846645, 121.481105, "INDONESIA", "aprile - ottobre", HueBlu));
			MarkerInfoDictionary.Add("m32", new MarkerInfo(2.382135, 76.813779, "SRI LANKA E MALDIVE", "gennaio - aprile", HueBlu));
			MarkerInfoDictionary.Add("m33", new MarkerInfo(15.947488, 100.469351, "THAILANDIA", "novembre - luglio", HueBlu));
			MarkerInfoDictionary.Add("m34", new MarkerInfo(4.638732, 102.139274, "MALESIA", "novembre - luglio", HueBlu));

			// VIOLA
			MarkerInfoDictionary.Add("m35", new MarkerInfo(-6.678904, 144.502549, "PAPUA NUOVA GUINEA", "maggio - ottobre", HueViola));
			MarkerInfoDictionary.Add("m36", new MarkerInfo(-17.365227, 133.646709, "AUSTRALIA NORD", "maggio - ottobre", HueViola));
			MarkerInfoDictionary.Add("m37", new MarkerInfo(-31.465597, 135.091100, "AUSTRALIA SUD", "novembre - maggio", HueViola));
			MarkerInfoDictionary.Add("m38", new MarkerInfo(-13.922528, -166.641066, "POLINESIA", "aprile - ottobre", HueViola));
			MarkerInfoDictionary.Add("m39", new MarkerInfo(-17.744841, 178.171009, "ISOLE FUJI", "maggio - ottobre", HueViola));
			MarkerInfoDictionary.Add("m40", new MarkerInfo(-43.091808, 171.844557, "NUOVA ZELANDA", "novembre - maggio", HueViola));


			mapFrag = FragmentManager.FindFragmentById<MapFragment>(Resource.Id.Map);
			mapFrag.GetMapAsync(this);

			// SEND BTN
			

            NomeEditTxt = FindViewById<EditText>(Resource.Id.NomeEditTxt);
            NomeEditTxt.Typeface = tfOR;

            CognomeEditTxt = FindViewById<EditText>(Resource.Id.CognomeEditTxt);
            CognomeEditTxt.Typeface = tfOR;

            EMailEditTxt = FindViewById<EditText>(Resource.Id.EMailEditTxt);
            EMailEditTxt.Typeface = tfOR;

            TelefonoEditTxt = FindViewById<EditText>(Resource.Id.TelefonoEditTxt);
            TelefonoEditTxt.Typeface = tfOR;

			SendBtn = FindViewById<LinearLayout>(Resource.Id.ButtonSendView);
			SendBtn.Click += (sender, e) =>
			{
                APIViaggioIdeale(DaDataTxt.Text, ADataTxt.Text,
                                 CognomeEditTxt.Text,NomeEditTxt.Text,TelefonoEditTxt.Text,EMailEditTxt.Text,
                                 ModalMarkerTitle);
			};


			// GESTIONE MODALE PREVENTIVO

			ModalPreventivo = FindViewById<RelativeLayout>(Resource.Id.ModalPreventivo);
			ModalPreventivo.Visibility = ViewStates.Gone;
			ModalPreventivo.Clickable = true;
		
			ModalPreventivo.Click += delegate {

				ModalPreventivo.Visibility = ViewStates.Gone;

			};

            var ModalPreventivoContainer = FindViewById<RelativeLayout>(Resource.Id.ModalPreventivoContainer);
            ModalPreventivoContainer.Clickable = true;

			// GESTIONE MODALE PICKER

			ModalDatePicker = FindViewById<RelativeLayout>(Resource.Id.ModalDatePicker);
			ModalDatePicker.Visibility = ViewStates.Gone;
			ModalDatePicker.Clickable = true;

			Date = FindViewById<DatePicker>(Resource.Id.Date);

			DateBtn = FindViewById<TextView>(Resource.Id.DateBtn);
			DateBtn.Typeface = tfOSB;
			DateBtn.Click += (sender, e) =>
			{
				ModalDatePicker.Visibility = ViewStates.Gone;

				if (DaOpen)
				{
					DaDate = new DateTime(Date.Year, Date.Month + 1, Date.DayOfMonth, 0, 0, 0);
					DaDataTxt.Text = FormatDateTime(DaDate);

					if (ADate.CompareTo(DaDate) < 0)
					{
						ADate = DaDate.AddDays(1);
						ADataTxt.Text = FormatDateTime(ADate);
					}
				}

				if (AOpen)
				{
					ADate = new DateTime(Date.Year, Date.Month + 1, Date.DayOfMonth, 0, 0, 0);
					ADataTxt.Text = FormatDateTime(ADate);
				}


				DaOpen = false;
				AOpen = false;
			};

			// INIT MENU

			menu = FindViewById<MenuView>(Resource.Id.menu);
			menu.SetBackgroundColor(Color.Argb(75, 0, 0, 0));
			menu.SetMenuColor(Color.Rgb(102, 104, 162));
			menu.PAGE_TAG = "3";
			menu.CellHeight = 50;
			menu.CellWidht = 225;
			menu.MenuImg = MenuImg;

			menu.AddElement(GbsInstance.ListMenuElement);

		}

		public override void OnBackPressed()
		{
			if (menu.MenuOpen)
			{
				menu.CloseMenu();
			}
			else
			{
				Finish();

			}

			//base.OnBackPressed()
		}

		public void OnMapReady(GoogleMap googleMap)
		{
			//throw new NotImplementedException();

			//Console.WriteLine("MAP READY:" + (googleMap != null));

			float maxZoom = 0;

			//LatLng location = TRUEPOS;
			CameraPosition.Builder builder = CameraPosition.InvokeBuilder();
			builder.Target(new LatLng(0, 0));
			builder.Zoom(maxZoom);
			CameraPosition cameraPosition = builder.Build();
			CameraUpdate cameraUpdate = CameraUpdateFactory.NewCameraPosition(cameraPosition);

			gMap = googleMap;

			if (gMap != null)
			{
				gMap.MapType = GoogleMap.MapTypeNormal;
				gMap.UiSettings.ZoomControlsEnabled = false;
				gMap.UiSettings.CompassEnabled = false;
				gMap.UiSettings.MyLocationButtonEnabled = false;
				gMap.MyLocationEnabled = false;
				gMap.UiSettings.MapToolbarEnabled = false;
				gMap.MoveCamera(cameraUpdate);
				gMap.SetMaxZoomPreference(maxZoom);
				gMap.SetMinZoomPreference(maxZoom);
				//BitmapDescriptor descriptor = BitmapDescriptorFactory.FromResource (Resource.Id.icon);

				/*
				MarkerOptions markerOpt = new MarkerOptions();
				markerOpt.SetPosition(new LatLng(0, 0));
				markerOpt.SetTitle("MARKER GIALLO");
				markerOpt.SetSnippet("per1 - per2");
				markerOpt.SetIcon(BitmapDescriptorFactory.DefaultMarker(49));
				var m = gMap.AddMarker(markerOpt);

				MarkerOptions markerOpt2 = new MarkerOptions();
				markerOpt2.SetPosition(new LatLng(0, 10));
				markerOpt2.SetTitle("MARKER VIOLA");
				markerOpt2.SetSnippet("per1 - per2");
				markerOpt2.SetIcon(BitmapDescriptorFactory.DefaultMarker(290));
				var m2 = gMap.AddMarker(markerOpt2);

				MarkerOptions markerOpt3 = new MarkerOptions();
				markerOpt3.SetPosition(new LatLng(10, 0));
				markerOpt3.SetTitle("MARKER ROSSO");
				markerOpt3.SetSnippet("per1 - per2");
				markerOpt3.SetIcon(BitmapDescriptorFactory.DefaultMarker(352));
				var m3 = gMap.AddMarker(markerOpt3);

				MarkerOptions markerOpt4 = new MarkerOptions();
				markerOpt4.SetPosition(new LatLng(-10, 0));
				markerOpt4.SetTitle("MARKER VERDE");
				markerOpt4.SetSnippet("per1 - per2");
				markerOpt4.SetIcon(BitmapDescriptorFactory.DefaultMarker(141));
				var m4 = gMap.AddMarker(markerOpt4);

				MarkerOptions markerOpt5 = new MarkerOptions();
				markerOpt5.SetPosition(new LatLng(0, -10));
				markerOpt5.SetTitle("MARKER ARANCIONE");
				markerOpt5.SetSnippet("per1 - per2");
				markerOpt5.SetIcon(BitmapDescriptorFactory.DefaultMarker(19));
				var m5 = gMap.AddMarker(markerOpt5);

				MarkerOptions markerOpt6 = new MarkerOptions();
				markerOpt6.SetPosition(new LatLng(10, 10));
				markerOpt6.SetTitle("MARKER BLU");
				markerOpt6.SetSnippet("per1 - per2");
				markerOpt6.SetIcon(BitmapDescriptorFactory.DefaultMarker(228));
				var m6 = gMap.AddMarker(markerOpt6);*/


				for (int c = 0; c < MarkerInfoDictionary.Count; c++)
				{
					MarkerInfo mi = MarkerInfoDictionary.ElementAt(c).Value;
					MarkerOptions markerOpt = new MarkerOptions();
					markerOpt.SetPosition(new LatLng((double)mi.Lat, (double)mi.Lon));
					markerOpt.SetTitle(mi.Title);
					markerOpt.SetSnippet(mi.Period);
					markerOpt.SetIcon(BitmapDescriptorFactory.DefaultMarker(mi.IconHue));
					var m = gMap.AddMarker(markerOpt);
					MarkerList.Add(m);

				}

				gMap.SetInfoWindowAdapter(new CustomMarkerPopupAdapter(LayoutInflater));
				gMap.InfoWindowClick += MapOnInfoWindowClick;
			}

		}

		private void MapOnInfoWindowClick(object sender, GoogleMap.InfoWindowClickEventArgs e)
		{
			Marker myMarker = e.Marker;
			//Console.WriteLine("MID:"+myMarker.Id+"|"+myMarker.Title);

            ModalMarkerTitle = myMarker.Title;
			ModalPreventivo.Visibility = ViewStates.Visible;

			/*
			MarkerInfo miCl = MarkerDictionary[myMarker.Id];
			if (miCl.nome == "PARCHEGGI")
			{
				//Console.WriteLine("geo:" + miCl.lat + "," + miCl.lon);
				string lat = miCl.lat.ToString().Replace(",", ".");
				string lon = miCl.lon.ToString().Replace(",", ".");
				//Console.WriteLine("geo:" + lat + "," + lon + "?q=" + WebUtility.UrlEncode(miCl.via));
				var geoUri = Android.Net.Uri.Parse("geo:" + miCl.lat + "," + miCl.lon + "?q=" + WebUtility.UrlEncode(miCl.via));
				var mapIntent = new Intent(Intent.ActionView, geoUri);
				StartActivity(mapIntent);
			}
			else
			{
				var intent = new Intent(this, typeof(CosaVedereInfoActivity));
				intent.PutExtra("key", myMarker.Id);
				StartActivity(intent);
			}

			*/
		}
	
		public string FormatDateTime(DateTime dt)
		{

			string formatted = "";

			formatted = dt.ToString("dd MMM yyyy");

			return formatted;

		}

		public long FromDateTime(DateTime time)
		{

			DateTime local = new DateTime(time.Year, time.Month, time.Day,
										  time.Hour, time.Minute, time.Second,
										  DateTimeKind.Local);
			DateTime utc = local.ToUniversalTime();

			var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

			long sec = (long)(utc - epoch).TotalSeconds;

			return sec;
		}


        public void APIViaggioIdeale(string data_partenza, string data_fine,string cognome,string nome,string telefono, string email, string localita){

            SendBtn.Clickable = false;
                      
            bool flagNome = false;
            bool flagEMail = false;
            //bool flagMessage = false;
            bool flagCognome = false;
            //bool flagTelefono = false;


            if (string.IsNullOrEmpty(nome))
            {
                flagNome = true;
            }
            if (string.IsNullOrEmpty(cognome))
            {
                flagCognome = true;
            }

            if (string.IsNullOrEmpty(email))
            {
                flagEMail = true;
            }
            else
            {
                try
                {
                    MailAddress m = new MailAddress(email);
                }
                catch (FormatException)
                {
                    flagEMail = true;
                }
            }


            if (flagEMail || flagNome  || flagCognome)
            {
                SendBtn.Clickable = true;
                Toast.MakeText(this, "Errore dati non inseriti o errati", ToastLength.Long).Show();
                return;

            }

            var client = new RestClient(GbsInstance.BaseUrl);

            var requestN4U = new RestRequest("mail_app.php", Method.POST);
            requestN4U.AddHeader("content-type", "application/json");
            //requestN4U.AddParameter("application/x-www-form-urlencoded", $"post={"3"}&email={"matteo.lomi@netwintec.com"}&nome={"Matteo"}&messaggio={"Test Messaggio"}", ParameterType.RequestBody);
            JObject oJsonObject = new JObject();

            oJsonObject.Add("tipo", "1");
            oJsonObject.Add("data_partenza", data_partenza);
            oJsonObject.Add("data_fine", data_fine);
            oJsonObject.Add("cognome", cognome);
            oJsonObject.Add("nome", nome);
            oJsonObject.Add("telefono", telefono);
            oJsonObject.Add("email", email);
            oJsonObject.Add("localita", localita);

            requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);
            //Console.WriteLine(client.BaseUrl + "" + requestN4U.Resource);

            client.ExecuteAsync(requestN4U,(response, arg2) => {
                
                //Console.WriteLine("StatusCode vulandra/mail_app.php:" + response.StatusCode +
                //                  "\nContent vulandra/mail_app.php:" + response.Content);

                if(response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    RunOnUiThread(() =>
                    {

                        SendBtn.Clickable = true;
                        ModalPreventivo.Visibility = ViewStates.Gone;
                        Toast.MakeText(this, "Email inviata con successo", ToastLength.Long).Show();

                    });
                }
                else
                {
                    RunOnUiThread(() =>
                    {

                        SendBtn.Clickable = true;
                        ModalPreventivo.Visibility = ViewStates.Gone;
                        Toast.MakeText(this, "Errore riprovare più tardi", ToastLength.Long).Show();

                    });
                }

            });

           
        }        

	}

	public class CustomMarkerPopupAdapter : Java.Lang.Object, GoogleMap.IInfoWindowAdapter
	{
		private LayoutInflater _layoutInflater = null;

		public IntPtr IJHandle { get { return IntPtr.Zero; } }

		public CustomMarkerPopupAdapter(LayoutInflater inflater)
		{
			//This constructor does hit a breakpoint and executes
			_layoutInflater = inflater;
		}

		public View GetInfoWindow(Marker marker)
		{
			//This never executes or hits a break point
			return null;
		}

		public View GetInfoContents(Marker marker)
		{

			Typeface tfOR = Typeface.CreateFromAsset(MainActivity.Instance.Assets, "fonts/OpenSans_Regular.ttf");
			Typeface tfOSB = Typeface.CreateFromAsset(MainActivity.Instance.Assets, "fonts/OpenSans_SemiBold.ttf");
			//This never executes or hits a break point
			var customPopup = _layoutInflater.Inflate(Resource.Layout.CustomMapInfoWindow, null);

			var titleTextView = customPopup.FindViewById<TextView>(Resource.Id.CustomMarkerTitle);
			if (titleTextView != null)
			{
				titleTextView.Typeface = tfOSB;
				titleTextView.Text = marker.Title;
			}

			var periodTextView = customPopup.FindViewById<TextView>(Resource.Id.CustomMarkerPeriod);
			if (periodTextView != null)
			{
				periodTextView.Typeface = tfOR;
				periodTextView.Text = marker.Snippet;
			}

			var clickTextView = customPopup.FindViewById<TextView>(Resource.Id.CustomMarkerClick);
			if (clickTextView != null)
			{
				clickTextView.Typeface = tfOR;
			}

			return customPopup;
		}

		public void Dispose()
		{

		}	}

}

/*GESTIONE FIRMA 


***** XML ******

            <xamarin.controls.SignaturePadView
				android:id="@+id/signatureView"
                android:layout_width="match_parent"
                android:layout_height="150dp"
                android:layout_centerInParent="true" />



***** CODE ******

var signature = FindViewById<Xamarin.Controls.SignaturePadView>(Resource.Id.signatureView);
			signature.Caption.Text = "Authorization Signature";
			signature.Caption.SetTypeface(tfOR, TypefaceStyle.Normal);
			signature.Caption.SetTextSize (global::Android.Util.ComplexUnitType.Dip, 16f);
			signature.Caption.SetTextColor(Color.Black);
			signature.BackgroundColor = Color.Rgb (255, 255, 200); // a light yellow.
			signature.StrokeColor = Color.Black;

			FindViewById<TextView>(Resource.Id.ViaggioIdealeTxt).Click += (sender, e) =>
			{
				//Console.WriteLine("CLICK");
				FindViewById<ImageView>(Resource.Id.ViaggioIdealeImg).SetImageBitmap(signature.GetImage());
				Bitmap btm = signature.GetImage();
				using (var stream = new System.IO.MemoryStream())
				{
					btm.Compress(Bitmap.CompressFormat.Png, 0, stream);

					var bytes = stream.ToArray();
					var str = Convert.ToBase64String(bytes);
				}
			};*/

