﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Widget;
using Android.OS;
using Android.Graphics;
using Android.Views;
using Android.Content.PM;
using Android.Content;
using Android.Text;

namespace Vulandra
{
	[Activity(ScreenOrientation = ScreenOrientation.Portrait)]
	public class ChiSiamoActivity : Activity
	{
		GlobalStructureInstance GbsInstance = GlobalStructureInstance.Instance;

		Typeface tfMB, tfOB, tfOR, tfOSB;

		RelativeLayout MenuClick,BackClick;
		ImageView MenuImg,BackImg;
		MenuView menu;

		string MainText = 
			"Vulandra Viaggi rappresenta una agenzia viaggi controtendenza perché si attiene a una filosofia di vendita che mira a turismo per addizione anziché un turismo per sottrazione." +
			"\nNegli ultimi anni il mondo commerciale è stato inondato solo da forme di sconto diverse cosa che però ha portato a una grossa perdita di qualità." +
			"\n\nVulandra Viaggi vi offre i migliori servizi ma anche:" +
			"\n__BOLDTEXT__" +
			"\n\nVulandra viaggi è l'agenzia che non ti aspetti dove il mio tempo è a tua disposizione per ascoltare le tue esperienze passate e le tue esigenze future con attenzione, rispetto e serietà." +
			"\n\nSolo attraverso la cura delle vostre necessità è possibile realizzare per ognuno di voi viaggi indimenticabili.\n\nTroverete le soluzioni più accurate per single, coppie, famiglie, gruppi di amici e per chi non vuole rinunciare alla compagnia degli inseparabili amici a quattro zampe.Un'attenzione particolare la rivolgiamo anche a occasioni speciali come anniversari, compleanni, lauree, ricorrenze e viaggi di nozze in collaborazione con i migliori professionisti nel campo dei servizi fotografici e dei trattamenti benessere.";

		string MainBoldText =
			"- Più competenza e professionalità" +
			"\n- Più esperienza e attenzione" +
			"\n- Più specializzazione" +
			"\n- Più scelta" +
			"\n- Più regali e gadget" +
			"\n- Più eventi"+
            "\n- Consulenza in LIS tramite videocall per sordi";

		public static ChiSiamoActivity Instance { private set; get;}

		protected override void OnResume()
		{
			GbsInstance.SetPage(PageName.ChiSiamo);

			base.OnResume();
		}

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			Window.RequestFeature(WindowFeatures.NoTitle);

			SetContentView(Resource.Layout.ChiSiamoLayout);

			GbsInstance.SetPage(PageName.ChiSiamo);

			Instance = this;

			tfMB = Typeface.CreateFromAsset(Assets, "fonts/Montserrat_Bold.ttf");
			tfOB = Typeface.CreateFromAsset(Assets, "fonts/OpenSans_Bold.ttf");
			tfOR = Typeface.CreateFromAsset(Assets, "fonts/OpenSans_Regular.ttf");
			tfOSB = Typeface.CreateFromAsset(Assets, "fonts/OpenSans_SemiBold.ttf");

			// Get our button from the layout resource,

			MenuClick = FindViewById<RelativeLayout>(Resource.Id.MenuClick);
			MenuImg = FindViewById<ImageView>(Resource.Id.MenuIcon);

			MenuClick.Click += delegate
			{
				if (menu.MenuOpen)
				{
					menu.CloseMenu();
				}
				else
				{
					menu.OpenMenu();
				}

			};

			BackClick = FindViewById<RelativeLayout>(Resource.Id.BackClick);
			BackImg = FindViewById<ImageView>(Resource.Id.BackIcon);

			BackClick.Click += delegate
			{
				if (!menu.MenuOpen)
				{
					Finish();
				}

			};


			FindViewById<TextView>(Resource.Id.ChiSiamoTxt).Typeface = tfMB;

			TextView MainTextView = FindViewById<TextView>(Resource.Id.MainTxt);
			MainTextView.Typeface = tfOR;

			string CompleteTxt = MainText.Replace("__BOLDTEXT__",MainBoldText);
			var ssIU = new SpannableString(CompleteTxt);

			int start = MainText.IndexOf("__BOLDTEXT__");

			//Console.WriteLine(start + "|" + MainBoldText.Length + "|" + (start + MainBoldText.Length) + "|" + CompleteTxt.Length);

			ssIU.SetSpan(new CustomFontSpan("normal", tfOR), 0, start - 1, SpanTypes.ExclusiveExclusive);
			ssIU.SetSpan(new CustomFontSpan("bold", tfOB), start, start + MainBoldText.Length, SpanTypes.ExclusiveExclusive);
			ssIU.SetSpan(new CustomFontSpan("normal", tfOR), start + MainBoldText.Length, CompleteTxt.Length, SpanTypes.ExclusiveExclusive);

			MainTextView.TextFormatted = ssIU;
		

			// INIT MENU

			menu = FindViewById<MenuView>(Resource.Id.menu);
			menu.SetBackgroundColor(Color.Argb(75, 0, 0, 0));
			menu.SetMenuColor(Color.Rgb(102, 104, 162));
			menu.PAGE_TAG = "1";
			menu.CellHeight = 50;
			menu.CellWidht = 225;
			menu.MenuImg = MenuImg;

			menu.AddElement(GbsInstance.ListMenuElement);

		}

		public override void OnBackPressed()
		{
			if (menu.MenuOpen)
			{
				menu.CloseMenu();
			}
			else
			{
				Finish();

			}

			//base.OnBackPressed()
		}
	
	}
}
